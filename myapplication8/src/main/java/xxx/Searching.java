package xxx;

public class Searching<T extends Comparable<T>>
{
    // 线性查找，顺序查找
    public static <T extends Comparable<? super T>>
         boolean linearSearch(T[] data, int min, int max, T target)
    {
        int index = min;
        boolean found = false;

        while (!found && index <= max)
        {
            found = data[index].equals(target);
            index++;
        }

        return found;
    }
    // 二分查找
    public static <T extends Comparable<? super T>>
          boolean binarySearch(T[] data, int min, int max, T target)
    {
        boolean found = false;
        int midpoint = (min + max) / 2;  // determine the midpoint

        if (data[midpoint].compareTo(target) == 0)
            found = true;

        else
            if (data[midpoint].compareTo(target) > 0)
            {
                if (min <= midpoint - 1)
                    found = binarySearch(data, min, midpoint - 1, target);
            }

        else
            if (midpoint + 1 <= max)
                found = binarySearch(data, midpoint + 1, max, target);

        return found;
    }

    //插值查找
    public static <T extends Comparable<? super T>>
       boolean insertionSearch(T[] data, int min, int max, T target)
    {
        boolean found = false;
        int midpoint = min+(target.compareTo(data[min]))/(data[max].compareTo(data[min]))*(max-min);
        if (data[midpoint].compareTo(target) == 0)
            found = true;

        else if (data[midpoint].compareTo(target) > 0)
        {
            if (min <= midpoint - 1)
                found = insertionSearch(data, min, midpoint - 1, target);
        }

        else if (midpoint + 1 <= max)
            found = insertionSearch(data, midpoint + 1, max, target);

        return found;
    }
    public static  int max_size=20;//斐波那契数组的长度

    /*构造一个斐波那契数组*/
    public static void Fibonacci(int[] F)
    {
        F[0]=0;
        F[1]=1;
        for(int i=2;i<max_size;++i)
            F[i]=F[i-1]+F[i-2];
    }

    //斐波那契查找
    public static <T extends Comparable<? super T>>
       boolean FibonacciSearch(T[] data, int min, int max, T target)
    {
        int length = max - min +1;

        int[] F = new int[max_size];
        Fibonacci(F);

        int k=0;
        while (length>(F[k]-1))
        {
            k++;
        }
        Object[] temp = new Object[max_size];
        System.arraycopy(data,0,temp,0,data.length-1);
        for (int i=length;i<F[k]-1;i++)
        {
            temp[i] = data[length-1];
        }
        while(min<=max)
        {
            int mid=min+F[k-1]-1;
            if(target.compareTo((T) temp[mid])<0)
            {
                max=mid-1;
                k-=1;
            }
            else if(target.compareTo((T)temp[mid])>0)
            {
                min=mid+1;
                k-=2;
            }
            else
            {
               return true;
            }
        }
        return false;
    }

    //二叉查找树——树表查找
    public static <T extends Comparable<? super T>>
    boolean binaryTreeSearch(T[] data, int min, int max, T target)
    {
        boolean found = false;
        LinkedBinarySearchTree linkedBinarySearchTree = new LinkedBinarySearchTree();
       // Comparable<T> comparableElement = (Comparable<T>)target;
        for (int i=min;i<=max;i++)
            linkedBinarySearchTree.addElement(data[i]);

        while (found==false&&linkedBinarySearchTree.root!=null) {
            if (target.equals(linkedBinarySearchTree.root.getElement())){
                found = true;
            }
            else
                {
                if (target.compareTo((T)linkedBinarySearchTree.root.getElement()) < 0)
                   linkedBinarySearchTree.root = linkedBinarySearchTree.root.left;
                else
                    linkedBinarySearchTree.root = linkedBinarySearchTree.root.getRight();
                }
        }
        return found;
    }

    // 分块查找
    public static <T extends Comparable<? super T>>
    boolean partitionSearch(T[] data, int min, int max, T target)
    {
        boolean result = false;
        int partition1 = partition(data,min,max);

        int partition2 = partition(data,min,partition1-1);
        int partition3 = partition(data,partition1+1,max);

        if (target.compareTo(data[partition1])==0)
            return true;
        else
            {
                if (target.compareTo(data[partition1])<0)
                {
                    if (target.compareTo(data[partition2])==0)
                        return true;
                    else
                    {
                        if (target.compareTo(data[partition2])<0)
                            result =  linearSearch(data,min,partition2-1,target);
                        else
                            result =  linearSearch(data,partition2+1,partition1-1,target);
                    }
                }
                else
                    {
                        if (target.compareTo(data[partition3])==0)
                            return true;
                        else
                        {
                            if (target.compareTo(data[partition3])<0)
                              result = linearSearch(data,partition1+1,partition3-1,target);
                            else
                                result = linearSearch(data,partition3+1,max,target);
                        }
                    }
            }
            return result;
    }


    private static <T extends Comparable<? super T>>
    int partition(T[] data, int min, int max)
    {
        T partitionelement;
        int left, right;
        int middle = (min + max) / 2;

        // use the middle data value as the partition element
        partitionelement = data[middle];
        // move it out of the way for now
        swap(data, middle, min);

        left = min;
        right = max;

        while (left < right)
        {
            // search for an element that is > the partition element
            while (left < right && data[left].compareTo(partitionelement) <= 0)
                left++;

            // search for an element that is < the partition element
            while (data[right].compareTo(partitionelement) > 0)
                right--;

            // swap the elements
            if (left < right)
                swap(data, left, right);
        }

        // move the partition element into place
        swap(data, min, right);

        return right;
    }
    private static <T extends Comparable<? super T>> void swap(T[] data, int index1, int index2)
    {
        T temp = data[index1];
        data[index1] = data[index2];
        data[index2] = temp;
    }

    // 哈希查找
    public static <T extends Comparable<? super T>>
    boolean hashSearch(int[] data, int min, int max, int target)
    {
        boolean result = false;
        hashConflict hashConflict = new hashConflict();
        for (int i=0;i<data.length;i++)
            hashConflict.add((int)data[i]);
        int index = target%hashConflict.number;
        while (hashConflict.list[index]!=null&&result==false)
        {
            if (hashConflict.list[index].getElement().equals(target))
                result = true;
            else
                hashConflict.list[index] = hashConflict.list[index].getNext();
        }


        return result;
    }



}
