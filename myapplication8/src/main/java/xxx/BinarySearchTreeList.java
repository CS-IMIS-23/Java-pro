package xxx;

import chapter6.jsjf.*;

import java.util.Iterator;

public class BinarySearchTreeList<T> extends LinkedBinarySearchTree<T>
        implements ListADT<T>, OrderedListADT<T>, Iterable<T> {
    public BinarySearchTreeList() {
        super();
    }

    @Override
    public void add(T element) {
        addElement(element);
    }

    @Override
    public T removeFirst() {
        return removeMin();
    }

    @Override
    public T removeLast() {
        return removeLast();
    }

    @Override
    public T remove(T element) {
        return removeElement(element);
    }

    @Override
    public T first() {
        return findMin();
    }

    @Override
    public T last() {
        return findMax();
    }

    public Iterator<T> iterator()
    {
        return iteratorInOrder();
    }
}
