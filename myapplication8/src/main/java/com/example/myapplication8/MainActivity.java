package com.example.myapplication8;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import  xxx.*;
import java.util.Stack;
import java.util.StringTokenizer;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText editText_input =(EditText)findViewById(R.id.editText);
        final EditText editText_input1 = (EditText)findViewById(R.id.editText2);
        final EditText editText_output =(EditText)findViewById(R.id.editText3);
        final EditText editText_output1 = (EditText)findViewById(R.id.editText4);

        Button button = (Button)findViewById(R.id.button);//线性
        Button button2 = (Button)findViewById(R.id.button2);//二分
        Button button3 = (Button)findViewById(R.id.button3);// 插值
        Button button4 = (Button)findViewById(R.id.button4);//斐波那契
        Button button5 = (Button)findViewById(R.id.button5);//树表
        Button button6 = (Button)findViewById(R.id.button6);//分块
        Button button7 = (Button)findViewById(R.id.button7);//哈希

        Button button8 = (Button)findViewById(R.id.button8);//选择排序
        Button button9 = (Button)findViewById(R.id.button9);//希尔排序
        Button button10 = (Button)findViewById(R.id.button10);//堆排序
        Button button11 = (Button)findViewById(R.id.button11);//二叉树排序


        button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String message = editText_input.getText().toString();
                StringTokenizer stringTokenizer = new StringTokenizer(message);
                String [] array = new String[stringTokenizer.countTokens()];
                int i =0;
                while (stringTokenizer.hasMoreTokens())
                {
                    array[i] = stringTokenizer.nextToken();
                    i++;
                }
                String x = editText_input1.getText().toString();

                boolean result = Searching.linearSearch(array,0,array.length-1,x);
                editText_output.setText(String.valueOf(result));
            }
        });
        button2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String message = editText_input.getText().toString();
                StringTokenizer stringTokenizer = new StringTokenizer(message);
                String [] array = new String[stringTokenizer.countTokens()];
                int i =0;
                while (stringTokenizer.hasMoreTokens())
                {
                    array[i] = stringTokenizer.nextToken();
                    i++;
                }
                String x = editText_input1.getText().toString();

                Sorting.selectionSort(array);
                boolean result = Searching.binarySearch(array,0,array.length-1,x);
                editText_output.setText(String.valueOf(result));
            }
        });
        button3.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String message = editText_input.getText().toString();
                StringTokenizer stringTokenizer = new StringTokenizer(message);
                String [] array = new String[stringTokenizer.countTokens()];
                int i =0;
                while (stringTokenizer.hasMoreTokens())
                {
                    array[i] = stringTokenizer.nextToken();
                    i++;
                }
                String x = editText_input1.getText().toString();
                Sorting.selectionSort(array);
                boolean result = Searching.insertionSearch(array,0,array.length-1,x);
                editText_output.setText(String.valueOf(result));
            }
        });
        button4.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String message = editText_input.getText().toString();
                StringTokenizer stringTokenizer = new StringTokenizer(message);
                String [] array = new String[stringTokenizer.countTokens()];
                int i =0;
                while (stringTokenizer.hasMoreTokens())
                {
                    array[i] = stringTokenizer.nextToken();
                    i++;
                }
                String x = editText_input1.getText().toString();
                Sorting.selectionSort(array);
                boolean result = Searching.FibonacciSearch(array,0,array.length-1,x);
                editText_output.setText(String.valueOf(result));
            }
        });
        button5.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String message = editText_input.getText().toString();
                StringTokenizer stringTokenizer = new StringTokenizer(message);
                String [] array = new String[stringTokenizer.countTokens()];
                int i =0;
                while (stringTokenizer.hasMoreTokens())
                {
                    array[i] = stringTokenizer.nextToken();
                    i++;
                }
                String x = editText_input1.getText().toString();

                boolean result = Searching.binaryTreeSearch(array,0,array.length-1,x);
                editText_output.setText(String.valueOf(result));
            }
        });
        button6.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String message = editText_input.getText().toString();
                StringTokenizer stringTokenizer = new StringTokenizer(message);
                String [] array = new String[stringTokenizer.countTokens()];
                int i =0;
                while (stringTokenizer.hasMoreTokens())
                {
                    array[i] = stringTokenizer.nextToken();
                    i++;
                }
                String x = editText_input1.getText().toString();

                boolean result = Searching.partitionSearch(array,0,array.length-1,x);
                editText_output.setText(String.valueOf(result));
            }
        });
        button7.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String message = editText_input.getText().toString();
                StringTokenizer stringTokenizer = new StringTokenizer(message);
                int [] array = new int[stringTokenizer.countTokens()];
                int i =0;
                while (stringTokenizer.hasMoreTokens())
                {
                    array[i] = Integer.parseInt(stringTokenizer.nextToken());
                    i++;
                }
                int x = Integer.parseInt( editText_input1.getText().toString());

                boolean result = Searching.hashSearch(array,0,array.length-1,x);
                editText_output.setText(String.valueOf(result));
            }
        });


        button8.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String message = editText_input.getText().toString();
                StringTokenizer stringTokenizer = new StringTokenizer(message);
                Integer[] array = new Integer[stringTokenizer.countTokens()];
                int i =0;
                while (stringTokenizer.hasMoreTokens())
                {
                    array[i] = Integer.parseInt(stringTokenizer.nextToken());
                    i++;
                }
                Sorting.selectionSort(array);
                String result = "";
                for (int j=0;j<array.length;j++)
                    result +=array[j]+" ";

                editText_output1.setText(result);
            }
        });
        button9.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String message = editText_input.getText().toString();
                StringTokenizer stringTokenizer = new StringTokenizer(message);
                Integer [] array = new Integer[stringTokenizer.countTokens()];
                int i =0;
                while (stringTokenizer.hasMoreTokens())
                {
                    array[i] = Integer.valueOf(stringTokenizer.nextToken());
                    i++;
                }
                Sorting.shellSort(array);
                String result = "";
                for (int j=0;j<array.length;j++)
                    result +=array[j]+" ";

                editText_output1.setText(result);
            }
        });
        button10.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String message = editText_input.getText().toString();
                StringTokenizer stringTokenizer = new StringTokenizer(message);
                Integer [] array = new Integer[stringTokenizer.countTokens()];
                int i =0;
                while (stringTokenizer.hasMoreTokens())
                {
                    array[i] = Integer.valueOf(stringTokenizer.nextToken());
                    i++;
                }
                Sorting.heapSort(array);
                String result = "";
                for (int j=0;j<array.length;j++)
                    result +=array[j]+" ";

                editText_output1.setText(result);
            }
        });
        button11.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String message = editText_input.getText().toString();
                StringTokenizer stringTokenizer = new StringTokenizer(message);
                Integer [] array = new Integer[stringTokenizer.countTokens()];
                int i =0;
                while (stringTokenizer.hasMoreTokens())
                {
                    array[i] = Integer.valueOf(stringTokenizer.nextToken());
                    i++;
                }
                Sorting.binaryTreeSort(array);
                String result = "";
                for (int j=0;j<array.length;j++)
                    result +=array[j]+" ";
                editText_output1.setText(result);
            }
        });
    }
}
