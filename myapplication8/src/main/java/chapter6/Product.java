package chapter6;

import java.io.Serializable;

public class Product implements Serializable,Comparable<Product>
{
    public String name;
    public double price;

    public Product(String name,double price)
    {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String toString()
    {
        String result;
        result = "Name: "+name+" Price:"+price;
        return result;
    }

    @Override
    public int compareTo(Product product) {
        if (product.price<price)
            return -1;
        else if (product.price>price)
            return 1;
        else
            return 0;
    }
}
