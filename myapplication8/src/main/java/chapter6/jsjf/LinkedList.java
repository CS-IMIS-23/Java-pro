package chapter6.jsjf;

import chapter6.jsjf.exceptions.*;


public abstract class LinkedList<T> implements ListADT<T>, Iterable<T> {
    protected int count;
    protected LinearNode<T> head, tail;
    protected int modCount;

    public LinkedList() {
        count = 0;
        head = tail = null;
        modCount = 0;
    }

    public T remove(T targetElement) throws EmptyCollectionException,
            ElementNotFoundException
    {
        if (isEmpty())
            throw new EmptyCollectionException("LinkedList");

        boolean found = false;
        LinearNode<T> previous = null;
        LinearNode<T> current = head;
        while (current != null && !found)
            if (targetElement.equals(current.getElement()))
                found = true;
            else
            {
                previous = current;
                current = current.getNext();
            }

        if (!found)
            throw new ElementNotFoundException("LinkedList");

        if (size() == 1)  // only one element in the list
            head = tail = null;
        else if (current.equals(head))  // target is at the head
            head = current.getNext();
        else if (current.equals(tail))  // target is at the tail
        {
            tail = previous;
            tail.setNext(null);
        }
        else  // target is in the middle
            previous.setNext(current.getNext());

        count--;
        modCount++;

        return current.getElement();
    }
    public T removeFirst() {
        if (isEmpty())
            throw new EmptyCollectionException("LinkedList");

        LinearNode<T> current = head;// 当前的结点

        if (size() == 1)
            head = tail = null;
        else
            head = current.getNext();

        count--;
        modCount++;
        return current.getElement();
    }
    public T removeLast() {
        if (isEmpty())
            throw new EmptyCollectionException("LinkedList");
        boolean found = false;

        LinearNode<T> previous = null;
        LinearNode<T> current = head;

        // 找最后的结点
        while (current != null && !found) {
            if (current.equals(tail)) {
                found = true;
            } else {
                previous = current;
                current = current.getNext();
            }
        }

        if (size() == 1)
            head = tail = null;
        else {
            tail = previous;
            tail.setNext(null);
        }
        count--;
        modCount++;

        return current.getElement();
    }
    public boolean contains(T target)
    {
        boolean found = false;
        LinearNode<T> current = head;
        while (current != null && !found)
            if (target.equals(current.getElement()))
                found = true;
            else
                current = current.getNext();

            return found;
    }
    public T first()
    {
        if (isEmpty())
            throw new EmptyCollectionException("LinkedList");

        LinearNode<T> current = head;
        return current.getElement();
    }
    public T last()
    {
        if (isEmpty())
            throw new EmptyCollectionException("LinkedList");

        LinearNode<T> current = tail;
        return current.getElement();
    }
    public boolean isEmpty(){
        return count==0;
    }

    public int size()
    {
        return count;
    }

    public String toString()
    {
        LinearNode<T> temp;
        temp = head;
        String result = "";
        while(temp != null)
        {
            result += temp.getElement() + " ";
            temp = temp.getNext();
        }
        return result;
    }

    public void Sorting()
    {

    }
}