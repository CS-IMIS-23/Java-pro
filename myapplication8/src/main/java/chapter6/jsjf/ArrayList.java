package chapter6.jsjf;

import chapter6.jsjf.exceptions.*;


import java.util.*;

public abstract class ArrayList<T> implements ListADT<T>,Iterable<T>
{
    private final static int DEFAULT_CAPACITY = 100;
    private final static int NOT_FOUND = -1;

    protected int rear;
    protected T[] list;
    protected int modCount;

    /**
     * Creates an empty list using the default capacity.
     */
    public ArrayList()
    {
        this(DEFAULT_CAPACITY);
    }

    /**
     * Creates an empty list using the specified capacity.
     *
     * @param initialCapacity the integer value of the size of the array list
     */
    public ArrayList(int initialCapacity)
    {
        rear = 0;
        list = (T[])(new Object[initialCapacity]);
        modCount = 0;
    }

    protected void expandCapacity()
    {
        list = Arrays.copyOf(list, list.length * 2);
    }

    public T remove(T element)
    {
        T result;
        int index = find(element);

        if (index == NOT_FOUND)
            throw new ElementNotFoundException("ArrayList");

        result = list[index];
        rear--;

        // shift the appropriate elements
        for (int scan=index; scan < rear; scan++)
            list[scan] = list[scan+1];

        list[rear] = null;
        modCount++;

        return result;
    }
    public boolean contains(T target)
    {
        return (find(target) != NOT_FOUND);
    }

    /**
     * Returns the array index of the specified element, or the
     * constant NOT_FOUND if it is not found.
     *
     * @param target the target element
     * @return the index of the target element, or the
     *         NOT_FOUND constant
     */
    private int find(T target)
    {
        int scan = 0;
        int result = NOT_FOUND;

        if (!isEmpty())
            while (result == NOT_FOUND && scan < rear)
                if (target.equals(list[scan]))
                    result = scan;
                else
                    scan++;

        return result;
    }

    public T removeFirst() {
        T result;
        int index = 0;

        if (rear == 0)
            throw new EmptyCollectionException("ArrayList");
        else {
            result = list[index];
            rear--;

            // shift the appropriate elements
            for (int scan=index; scan < rear; scan++)
                list[scan] = list[scan+1];

            list[rear] = null;
            modCount++;

            return result;
        }
    }


    public T removeLast() {
        T result;
        int index = rear;

        if (rear == 0)
            throw new EmptyCollectionException("ArrayList");
        else {
            result = list[index];
            rear--;

            list[rear] = null;
            modCount++;

            return result;
        }
    }


    public T first() {
            T result;
            int index = 0;

            if (rear == 0)
                throw new EmptyCollectionException("ArrayList");
            else {
                result = list[index];
                return result;
            }
    }


    public T last() {
        T result;
        int index = rear;

        if (rear == 0)
            throw new EmptyCollectionException("ArrayList");
        else {
            result = list[index];
            return result;
        }
    }


    public boolean isEmpty() {
        return rear==0;
    }


    public int size() {
        return rear;
    }

    public String toString()
    {
        String result = "";
        for (int a = 0; a<rear;a++)
        {
            result += list[a] + " ";
        }
        return result;
    }

    @Override
    public Iterator<T> iterator() {
        return new ArrayListIterator();
    }

    private class ArrayListIterator implements Iterator<T> {
        int iteratorModCount;
        int current;
        public ArrayListIterator() {
            iteratorModCount = modCount;
            current = 0;
        }
        public boolean hasNext() throws ConcurrentModificationException {
            if (iteratorModCount != modCount)
                throw new ConcurrentModificationException();
            return (current < rear);
        }
        public T next() throws ConcurrentModificationException {
            if (!hasNext())
                throw new NoSuchElementException();

            current++;

            return list[current - 1];
        }
        public void remove() throws UnsupportedOperationException {
            throw new UnsupportedOperationException();
        }
    }

}
