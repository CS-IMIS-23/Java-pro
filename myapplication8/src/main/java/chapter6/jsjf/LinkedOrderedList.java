package chapter6.jsjf;

import java.util.Iterator;

public class LinkedOrderedList<T>  extends LinkedList<T> implements OrderedListADT<T>
{
    public LinkedOrderedList()
    {
        super();
    }


    public void add(T element)
    {
        LinearNode<T> temp = new LinearNode(element);
        LinearNode<T> previous = null;
        LinearNode<T> current = head;
        while(current != null && Integer.parseInt(element+"") > Integer.parseInt(current.getElement()+"")){
            previous = current;
            current = current.getNext();
        }

        if(previous == null){
            head = tail =  temp;
        }
        else{
            previous.setNext(temp);
        }
        temp.setNext(current);
        if(temp.getNext() == null)
            tail = temp;

        count++;
        modCount++;

    }

    @Override
    public Object remove(Object element) {
        return null;
    }

    @Override
    public boolean contains(Object target) {
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }

}

