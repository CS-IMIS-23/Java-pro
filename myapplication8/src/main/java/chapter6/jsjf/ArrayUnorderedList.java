package chapter6.jsjf;

import chapter6.jsjf.exceptions.ElementNotFoundException;
import chapter6.jsjf.exceptions.EmptyCollectionException;

public class ArrayUnorderedList<T> extends ArrayList<T> implements UnorderedListADT<T>
{
    /**
     * Creates an empty list using the default capacity.
     */
    public ArrayUnorderedList()
    {
        super();
    }

    /**
     * Creates an empty list using the specified capacity.
     *
     * @param initialCapacity the intial size of the list
     */
    public ArrayUnorderedList(int initialCapacity)
    {
        super(initialCapacity);
    }

    /**
     * Adds the specified element to the front of this list.
     *
     * @param element the element to be added to the front of the list
     */
    public void addToFront(T element)
    {
        if (size() == list.length) {
            expandCapacity();
        }
        int scan = 0;
        // 将现有元素向上移动1个
        for (int shift=rear; shift > scan; shift--) {
            list[shift] = list[shift - 1];
        }
        // 插入元素
        list[scan] = element;
        rear++;
        modCount++;
    }

    /**
     * Adds the specified element to the rear of this list.
     *
     * @param element the element to be added to the list
     */
    public void addToRear(T element)
    {
        if (size() == list.length) {
            expandCapacity();
        }
        list[rear] = element;
        rear++;
        modCount++;
    }

    public void addAfter(T element, T target)
    {
        if (size() == list.length)
            expandCapacity();

        int scan = 0;

        // find the insertion point
        while (scan < rear && !target.equals(list[scan]))
            scan++;

        if (scan == rear)
            throw new ElementNotFoundException("UnorderedList");

        scan++;

        // shift elements up one
        for (int shift=rear; shift > scan; shift--)
            list[shift] = list[shift-1];

        // insert element
        list[scan] = element;
        rear++;
        modCount++;
    }

    @Override
    public T removeFirst() {
        T result;
        int index = 0;

        if (rear == 0)
            throw new EmptyCollectionException("ArrayList");
        else {
            result = list[index];
            rear--;

            // shift the appropriate elements
            for (int scan=index; scan < rear; scan++)
                list[scan] = list[scan+1];

            list[rear] = null;
            modCount++;

            return result;
        }
    }
}
