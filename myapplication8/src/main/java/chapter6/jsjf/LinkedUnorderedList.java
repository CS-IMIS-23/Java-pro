package chapter6.jsjf;

import java.util.Iterator;

public class LinkedUnorderedList<T> extends LinkedList<T> implements UnorderedListADT<T> {
        @Override
        public void addToFront(T element) {
            LinearNode<T> node = new LinearNode<T>(element);
            LinearNode<T> current = head;
            if (isEmpty())
            {
                head = node;
            }
            else
            {
                node.setNext(current);
                head = node;
            }

            count++;
            modCount++;
        }

        @Override
        public void addToRear(T element) {
            LinearNode<T> node = new LinearNode<T>(element);

            node.setNext(null);
            if (isEmpty())
            {
                head = node;
            }
            else
            {
                tail.setNext(node);
            }
            tail = node;
            count++;
            modCount++;
        }

        @Override
        public void addAfter(T element, T target) {
            LinearNode<T> current = head;
            LinearNode<T> node = new LinearNode<>(element);

            if (isEmpty())
            {
                head = node;
            }

            while ((current!= null) && (current.getElement().equals(element)))
            {
                current = current.getNext();
            }
            if(current.getNext() == null){
                current.setNext(node);
                tail = node;
            }else{
                node.setNext(current.getNext());
                current.setNext(node);
            }

            count++;
            modCount++;
        }

    @Override
    public Iterator<T> iterator() {
        return null;
    }
}
