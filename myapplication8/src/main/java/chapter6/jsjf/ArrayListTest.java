package chapter6.jsjf;

public class ArrayListTest
{
    public static void main(String[] args) {
        System.out.println("测试ArrayOrderedList类");
        ArrayOrderedList<Integer> arrayOrderedList = new ArrayOrderedList(10);//测试ArrayOrderedList类
        arrayOrderedList.add(1);
        arrayOrderedList.add(2);
        arrayOrderedList.add(3);
        System.out.println("添加三个元素："+arrayOrderedList);
        System.out.println("列表中是否包含4？"+arrayOrderedList.contains(4));

        System.out.println();
        System.out.println("测试ArrayUnorderedList类");

        ArrayUnorderedList<Integer> arrayUnorderedList = new ArrayUnorderedList(10);//测试ArrayUnorderedList类
        arrayUnorderedList.addToFront(1);
        arrayUnorderedList.addToFront(2);
        arrayUnorderedList.addToFront(3);
        System.out.println("添加三个元素："+arrayUnorderedList);
        System.out.print("将4添加到列表最后：");
        arrayUnorderedList.addToRear(4);
        System.out.println(arrayUnorderedList);
        System.out.print("在2后面加入元素6：");
        arrayUnorderedList.addAfter(6,2);
        System.out.println(arrayUnorderedList);
        arrayUnorderedList.removeFirst();
        System.out.println(arrayUnorderedList);

    }
}
