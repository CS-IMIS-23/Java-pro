package chapter6.jsjf;

public class LinkedListTest
{
    public static void main(String[] args) {
        //有序列表的测试
        System.out.println("有序列表的测试");
        LinkedOrderedList ol = new LinkedOrderedList();
        ol.add(3);
        ol.add(9);
        ol.add(1);
        ol.add(6);
        ol.add(0);
        System.out.println("输出有序列表"+ol.toString());

        //无序列表的测试
        LinkedUnorderedList lul = new LinkedUnorderedList();

        System.out.println("在队列后端插入");
        lul.addToRear("3");
        System.out.println("输出无序列表：" + lul.toString());


        System.out.println("无序列表的测试");
        System.out.println("在队列前端插入");
        lul.addToFront("1");
        System.out.println("输出无序列表：" + lul.toString());

        lul.addToFront("2");
        System.out.println("输出无序列表：" + lul.toString());


        lul.addAfter("4","3");
        System.out.println("输出无序列表：" + lul.toString());
    }
}
