package chapter6;

import chapter6.jsjf.LinearNode;
import chapter6.jsjf.ListADT;
import chapter6.jsjf.exceptions.ElementNotFoundException;
import chapter6.jsjf.exceptions.EmptyCollectionException;

import java.util.Iterator;

public class StoreList<Product> implements ListADT<Product> {
    protected int count;
    protected LinearNode<Product> head, tail;


    public void add(Product product) {
        LinearNode<Product> linearNode = new LinearNode<>(product);
        if (isEmpty())
            head = linearNode;
        else
            tail.setNext(linearNode);

        tail = linearNode;
        count++;
    }

    @Override
    public Product removeFirst() {
        if (isEmpty())
            throw new EmptyCollectionException("LinkedList");

        LinearNode<Product> current = head;// 当前的结点

        if (size() == 1)
            head = tail = null;
        else
            head = current.getNext();

        count--;
        return current.getElement();
    }

    @Override
    public Product removeLast() {
        if (isEmpty())
            throw new EmptyCollectionException("LinkedList");
        boolean found = false;

        LinearNode<Product> previous = null;
        LinearNode<Product> current = head;

        // 找最后的结点
        while (current != null && !found) {
            if (current.equals(tail)) {
                found = true;
            } else {
                previous = current;
                current = current.getNext();
            }
        }

        if (size() == 1)
            head = tail = null;
        else {
            tail = previous;
            tail.setNext(null);
        }
        count--;


        return current.getElement();
    }

    @Override
    public Product remove(Product targetElement) throws EmptyCollectionException,
            ElementNotFoundException {
        if (isEmpty())
            throw new EmptyCollectionException("LinkedList");

        boolean found = false;
        LinearNode<Product> previous = null;
        LinearNode<Product> current = head;
        while (current != null && !found)
            if (targetElement.equals(current.getElement()))
                found = true;
            else {
                previous = current;
                current = current.getNext();
            }

        if (!found)
            throw new ElementNotFoundException("LinkedList");

        if (size() == 1)  // only one element in the list
            head = tail = null;
        else if (current.equals(head))  // target is at the head
            head = current.getNext();
        else if (current.equals(tail))  // target is at the tail
        {
            tail = previous;
            tail.setNext(null);
        } else  // target is in the middle
            previous.setNext(current.getNext());

        count--;

        return current.getElement();
    }

    @Override
    public Product first() {
        if (isEmpty())
            throw new EmptyCollectionException("LinkedList");

        LinearNode<Product> current = head;
        return current.getElement();
    }

    @Override
    public Product last() {
        if (isEmpty())
            throw new EmptyCollectionException("LinkedList");

        LinearNode<Product> current = tail;
        return current.getElement();
    }

    @Override
    public boolean contains(Product target) {
        boolean found = false;
        LinearNode<Product> current = head;
        while (current != null && !found)
            if (target.equals(current.getElement()))
                found = true;
            else
                current = current.getNext();

        return found;
    }


    @Override
    public boolean isEmpty() {
        return count == 0;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public Iterator<Product> iterator() {
        return null;
    }

    public String toString() {
        String result = "";
        LinearNode temp;
        temp = head;

        while (temp != null) {
            result += temp.getElement() + "     ";
            temp = temp.getNext();
        }
        return result;

    }

    public void Sorting() {
        LinearNode<Product> current = head;

        while (current.getNext()!=null) {
            LinearNode<Product> temp = current.getNext();
            Product min = current.getElement();
            while (temp != null) {
                if (current.compareTo((chapter6.Product) temp.getElement()) > 0)
                {
                    min = temp.getElement();
                }
                temp = temp.getNext();
            }

            LinearNode<Product> current0 = current;

            while (current0.getElement() != min) {
                current0 = current0.getNext();
            }
            current0.setElement(current.getElement());
            current.setElement(min);

            current = current.getNext();
        }

        String result = "";
        int i = 0;
        LinearNode<Product> linearNode = head;
        while (i<count)
        {
            result += linearNode.getElement()+"     ";
            linearNode = linearNode.getNext();
            i++;
        }
        System.out.println("排序后为："+result );

    }
}
