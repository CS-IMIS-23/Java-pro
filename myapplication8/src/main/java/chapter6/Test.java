package chapter6;

public class Test
{
    public static void main(String[] args) {
        StoreList storeList = new StoreList();
        Product product = new Product("bag",213);

        storeList.add(new Product("pen",12));
        storeList.add(new Product("book",57));
        storeList.add(product);
        storeList.add(new Product("food",34));
        storeList.add(new Product("water",3));
        System.out.println("显示当前商店中所有商品及价格："+storeList);

        System.out.println("查找商店中是否有背包商品："+storeList.contains(product));

        storeList.remove(product);

        System.out.println("删除背包商品后显示："+storeList);

        System.out.print("对商店商品按价格进行排序");
        storeList.Sorting();
    }
}
