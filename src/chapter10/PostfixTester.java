package chapter10;

import chapter10.jsjf.BinaryTreeNode;

import java.util.Scanner;

public class PostfixTester
{
    public static void main(String[] args)
    {
        String expression, again;
        int result;

        Scanner in = new Scanner(System.in);

        PostfixEvaluator evaluator = new PostfixEvaluator();
        do
        {
            System.out.println("Enter a valid post-fix expression one token " +
                    "at a time with a space between each token (e.g. 5 4 + 3 2 1 - + *)");
            System.out.println("Each token must be an integer or an operator (+,-,*,/)");
            expression = in.nextLine();

            result = evaluator.evaluate(expression);
            System.out.println();
            System.out.println("That expression equals " + result);

            System.out.println("The Expression Tree for that expression is: ");
            System.out.println(evaluator.getTree());

            System.out.print("Evaluate another expression [Y/N]? ");
            again = in.nextLine();
            System.out.println();

            ExpressionTreeOp expressionTreeOp = new ExpressionTreeOp(1,'+',0);
            System.out.println("树中是否含指定元素数字1："+evaluator.contains(expressionTreeOp));


            BinaryTreeNode<ExpressionTreeOp> binaryTreeNode = evaluator.getRoot();
            System.out.println(binaryTreeNode.isLeaf());


//            evaluator.removeAllElements();
//            System.out.println("删除树中所有元素后打印树："+evaluator.getTree());

        }
        while (again.equalsIgnoreCase("y"));

        System.out.println(evaluator.toString());


    }
}
