package chapter10;

import chapter10.jsjf.BinaryTreeNode;
import chapter10.jsjf.LinkedBinaryTree;
import chapter6.jsjf.ArrayUnorderedList;

import java.io.*;
import java.util.*;

public class DecisionTree
{
    private LinkedBinaryTree<String> tree;

    /**
     * Builds the decision tree based on the contents of the given file
     *
     * @param filename the name of the input file
     * @throws FileNotFoundException if the input file is not found
     */
    public DecisionTree(String filename) throws FileNotFoundException
    {
        File inputFile = new File(filename);
        Scanner scan = new Scanner(inputFile);
        int numberNodes = scan.nextInt();
        scan.nextLine();
        int root = 0, left, right;

        List<LinkedBinaryTree<String>> nodes = new ArrayList<LinkedBinaryTree<String>>();
        for (int i = 0; i < numberNodes; i++)
            nodes.add(i,new LinkedBinaryTree<String>(scan.nextLine()));
        while (scan.hasNext())
        {
            root = scan.nextInt();
            left = scan.nextInt();
            right = scan.nextInt();
            scan.nextLine();

            nodes.set(root, new LinkedBinaryTree<String>((nodes.get(root)).getRootElement(), nodes.get(left), nodes.get(right)));
        }
        tree = nodes.get(root);
    }
    public LinkedBinaryTree<String> getTree()
    {
        return tree;
    }

    public void evaluate()
    {
        LinkedBinaryTree<String> current = tree;
        Scanner scan = new Scanner(System.in);

        while (current.size() > 1)
        {
            System.out.println (current.getRootElement());
            if (scan.nextLine().equalsIgnoreCase("N"))
                current = current.getLeft();
            else
                current = current.getRight();

        }

       System.out.println (current.getRootElement());
    }

    public void levelOrder(LinkedBinaryTree<String> linkedBinaryTree) {
        if (linkedBinaryTree == null) {
            return;
        }

        int depth = linkedBinaryTree.getHeight();

        for (int i = 1; i <= depth; i++) {
            levelOrder(linkedBinaryTree, i);
            System.out.println("\n\n");
        }
    }

    private void levelOrder(LinkedBinaryTree<String> linkedBinaryTree, int level) {
        if (linkedBinaryTree == null || level < 1) {
            return;
        }

        if (level == 1) {
            System.out.print(linkedBinaryTree.getRootElement() + "  ");
            return;
        }

        // 左子树
        levelOrder(linkedBinaryTree.getLeft(), level - 1);

        // 右子树
        levelOrder(linkedBinaryTree.getRight(), level - 1);
    }

    private static int count = 0;
    public int CountLeaf ()
    {
        count = 0;
        CountLeaf(tree);
       return count;
    }


    private void CountLeaf(LinkedBinaryTree<String> tree)
    {
        if (!(tree == null))
        {
            if ((tree.getLeft() == null) && (tree.getRight() == null))
            {
                count++;
            }

            CountLeaf(tree.getLeft());
            CountLeaf(tree.getRight());
        }
    }

    public int getHeight()
    {
        return height(tree.getRootNode());
    }


    private int height(BinaryTreeNode<String> node)
    {
        if(node==null){
            return 0;
        }
        else {
            int leftTreeHeight = height(node.getLeft());
            int rightTreeHeight= height(node.getRight());
            return leftTreeHeight>rightTreeHeight ? (leftTreeHeight+1):(rightTreeHeight+1);
        }
    }



}
