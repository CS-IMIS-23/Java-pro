package chapter10;

import java.io.FileNotFoundException;

public class BackPainAnalyzer
{
    /**
     *  Asks questions of the user to diagnose a medical problem.
     */
    public static void main (String[] args) throws FileNotFoundException
    {
        System.out.println ("So, you're having back pain.");

        DecisionTree expert = new DecisionTree("D:\\JAVA程序设计\\Java-pro\\input.txt");
        expert.evaluate();
        int count =  expert.CountLeaf();
        System.out.println("叶子结点数为："+count);
//        expert.levelOrder(expert.getTree());

        System.out.println("树的高度为："+expert.getHeight());

    }
}
