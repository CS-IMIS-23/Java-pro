package week2;

import jsjf.QueueADT;
import jsjf.exceptions.EmptyCollectionException;

public class CircularArrayQueue<T> implements QueueADT<T> {
    private final static int DEFAULT_CAPACITY = 100;
    private int front, rear, count;
    private T[] queue;

    /**
     * Creates an empty queue using the specified capacity.
     * @param initialCapacity the initial size of the circular array queue
     */
    public CircularArrayQueue (int initialCapacity)
    {
        front = rear = count = 0;
        queue = (T[]) (new Object[initialCapacity]);
    }

    /**
     * Creates an empty queue using the default capacity.
     */
    public CircularArrayQueue()
    {
        this(DEFAULT_CAPACITY);
    }
    @Override
    public void enqueue(T element) {
        if (size() == queue.length)
            expandCapacity();

        queue[rear] = element;
        rear = (rear+1) % queue.length;

        count++;
    }
    private void expandCapacity()
    {
        T[] larger = (T[]) (new Object[queue.length *2]);

        for (int scan = 0; scan < count; scan++)
        {
            larger[scan] = queue[front];
            front = (front + 1) % queue.length;
        }

        front = 0;
        rear = count;
        queue = larger;
    }

    @Override
    public T dequeue() throws EmptyCollectionException{
        if (isEmpty())
            throw new EmptyCollectionException("queue");

        T result = queue[front];
        queue[front] = null;
        front = (front+1) % queue.length;

        count--;

        return result;
    }

    @Override
    public T first() {
        if (isEmpty())
            throw new EmptyCollectionException("queue");

        T result = queue[front];
        return result;
    }

    @Override
    public boolean isEmpty() {
       return count == 0;
    }

    @Override
    public int size() {
        return count;
    }

    public String toString() {
        String result = "";
        int scan = 0;

        while (scan < count) {
            if (queue[scan] != null) {
                result += queue[scan].toString() + "\n";
            }
            scan++;
        }

        return result;
    }
}
