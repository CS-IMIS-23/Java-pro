package week2;

import jsjf.*;
import jsjf.exceptions.EmptyCollectionException;

public class LinkedQueue<T> implements QueueADT<T>
{
    private int count;
    private LinearNode<T> head,tail;

    public LinkedQueue()
    {
        count = 0 ;
        head = tail = null;
    }

    @Override
    public void enqueue(T element) {

            LinearNode<T> node = new LinearNode<T>(element);

            if (isEmpty())
                head = node;
            else
                tail.setNext(node);

            tail = node;
            count++;


    }

    @Override
    public T dequeue() throws EmptyCollectionException
    {
        if (isEmpty())
            throw new EmptyCollectionException("queue");

        T result = head.getElement();
        head = head.getNext();
        count--;

        if (isEmpty())
            tail = null;

        return result;
    }

    @Override
    public T first() {
        if (isEmpty())
            throw new EmptyCollectionException("queue");

        T result = head.getElement();

        return result;
    }

    @Override
    public boolean isEmpty() {
        return count == 0;
    }

    @Override
    public int size() {
        return count;
    }

    public String toString() {
        String result = "";
        LinearNode<T> current = head;

        while (current != null) {
            result = result + (current.getElement()).toString() + "\n";
            current = current.getNext();
        }

        return result;
    }
}
