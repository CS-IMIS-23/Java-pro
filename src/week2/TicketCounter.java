package week2;

import java.util.*;

public class TicketCounter
{
    final static int PROCESS = 120;// /处理时间
    final static int MAX_CASHIERS = 10;//最大收银人数
    final static int NUM_CUSTOMERS = 100;

    public static void main(String[] args)
    {
        Customer customer;
        Queue<Customer> customerQueue = new LinkedList<Customer>();

        int[] cashierTime = new int[MAX_CASHIERS];
        //收银员的时间标记
        int totalTime, averageTime, departs,start;

        /** process the simulation for various number of cashiers */
        for (int cashiers = 0; cashiers < MAX_CASHIERS; cashiers++)
        {
            /** set each cashiers time to zero initially */
            for (int count = 0; count < cashiers; count++)
                cashierTime[count] = 0;

            /** load customer queue */
            for (int count = 1; count <= NUM_CUSTOMERS; count++)
                customerQueue.offer(new Customer(count * 15));

            totalTime = 0;//使用的总体时间

            /** process all customers in the queue */
            while (!(customerQueue.isEmpty()))
            {
                for (int count = 0; count <= cashiers; count++)
                {
                    if (!(customerQueue.isEmpty()))
                    {
                        customer = customerQueue.poll();
                        if (customer.getArrivalTime() > cashierTime[count])
                            start = customer.getArrivalTime() ;
                        else
                            start = cashierTime[count];

                        // 离开时间的设置
                        departs = start + PROCESS;
                        customer.setDepartureTime(departs);
                        cashierTime[count] = departs;
                        //每个顾客使用的最总时间
                        totalTime += customer.totalTime();

                    }
                }
            }

            averageTime = totalTime / NUM_CUSTOMERS;
            System.out.println("Number of cashiers: " + (cashiers + 1));
            System.out.println("Average time: " + averageTime + "\n");
        }
    }
}
