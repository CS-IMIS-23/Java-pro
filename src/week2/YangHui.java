package week2;

import java.util.Scanner;

public class YangHui
{
    public static void main(String[] args) {
        int n ;
        Scanner scanner = new Scanner(System.in);

        System.out.println("请输入打印行数：");
        n = scanner.nextInt();

        CircularArrayQueue<Integer> circularArrayQueue = new CircularArrayQueue<>(n);

        for (int i = 1;i<=n+1;i++)
        {
            if (i==1)
            {
                circularArrayQueue.enqueue(1);
            }
            else
            {
                for (int j = 1;j<=i;j++)
                {
                    if (j==1)
                    {
                        circularArrayQueue.enqueue(1);
                    }
                    else
                    {
                        if (j==i)
                        {
                            circularArrayQueue.enqueue(1);
                            System.out.print(circularArrayQueue.dequeue());
                            System.out.println();
                        }
                        else
                        {
                            int num = circularArrayQueue.dequeue();
                            System.out.print(num+" ");
                            circularArrayQueue.enqueue(num+circularArrayQueue.first());
                        }

                    }
                }
            }
        }
    }
}
