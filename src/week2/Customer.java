package week2;

public class Customer
{
    private int arrivalTime;
    private int departureTime;

    public Customer(int arrives) {
        this.arrivalTime = arrives;
        this.departureTime = 0;
    }

    public int getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(int arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public int getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(int departureTime) {
        this.departureTime = departureTime;
    }

    public int totalTime(){
        return departureTime-arrivalTime;
    }
}
