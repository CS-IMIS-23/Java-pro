package week2;

public class DequeTest
{
    public static void main(String[] args) {
        LinkedDeque linkedDeque = new LinkedDeque();
        linkedDeque.addLast(2);
        linkedDeque.addLast(4);
        linkedDeque.addLast(3);
        linkedDeque.addFirst(1);

        System.out.println(linkedDeque.toString());
    }
}
