package week2;

import java.util.*;

public class Codes {
    public static void main(String[] args) {

        int[] key = {5, 12, -3, 8, -9, 4, 10};

        Integer keyValue;
        String encode = "";
        String decode = "";

        String message = "All programmers are playwrights and all "
                + "computers are lousy actors.";

        Queue<Integer> encodingQueue = new LinkedList<Integer>();
        Queue<Integer> decodingQueue = new LinkedList<Integer>();

        /**
         * 初始化密钥队列
         */
        for (int i = 0; i < key.length; i++) {
            encodingQueue.add(key[i]);
            decodingQueue.add(key[i]);
        }

        /**
         *encode message
         */
        for (int i = 0; i < message.length(); i++) {
            keyValue = encodingQueue.remove();
            encode += (char) ((int) message.charAt(i) + keyValue.intValue());
            encodingQueue.add(keyValue);
        }
        System.out.println("Encode Message :\n" + encode);

        /**
         *decode message
         */
        for (int i = 0; i < encode.length(); i++) {
            keyValue = decodingQueue.remove();
            decode += (char) ((int) encode.charAt(i) - keyValue.intValue());
            decodingQueue.add(keyValue);
        }

        System.out.println("Encode Message :\n" + decode);
//        System.out.println(encode.length()==message.length());
    }
}
