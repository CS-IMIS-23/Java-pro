package week2;

import jsjf.DequeADT;
import jsjf.LinearNode;
import jsjf.NewLinearNode;
import jsjf.QueueADT;

public class LinkedDeque<T> implements DequeADT<T>
{
    private int count;
    private NewLinearNode<T> head,tail;

    public LinkedDeque()
    {
        count = 0 ;
        head = tail = null;
    }

    @Override
    public void addFirst(T element) {
        NewLinearNode<T> node = new NewLinearNode<T>(element);
        if (isEmpty())
            head = node;
        else
            node.setNext(head);

        head = node;
        count++;
    }

    @Override
    public void addLast(T element) {
        NewLinearNode<T> node = new NewLinearNode<T>(element);
        if (isEmpty())
            head = node;
        else
            tail.setNext(node);
        tail = node;
        count++;
    }

    @Override
    public T removeFirst() {
        return null;
    }

    @Override
    public T removeLast() {
        return null;
    }

    @Override
    public T first() {
        return null;
    }

    @Override
    public T last() {
        return null;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public int size() {
        return 0;
    }

    public String toString()
    {
        String result = "";
        NewLinearNode<T> current = head;

        while (current != null) {
            result = result + (current.getElement()).toString() + "\n";
            current = current.getNext();
        }

        return result;
    }
}