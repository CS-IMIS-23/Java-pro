package chapter15;

public class CityNetworkTest
{
    public static void main(String[] args) {
        CityNetwork<String> cityNetwork = new CityNetwork<>();
        cityNetwork.addCity("沈阳");
        cityNetwork.addCity("北京");
        cityNetwork.addCity("天津");
        cityNetwork.addCity("南京");
        cityNetwork.addCity("杭州");

        cityNetwork.addEdge(0,1,100);
        cityNetwork.addEdge(0,2,300);
        cityNetwork.addEdge(1,2,50);
        cityNetwork.addEdge(1,3,200);
        cityNetwork.addEdge(2,4,100);
        cityNetwork.addEdge(3,4,50);
        cityNetwork.addEdge(0,4,500);

        System.out.println(cityNetwork.toString());
        cityNetwork.getShortestWeightPath("沈阳","杭州");
        cityNetwork.getShortestPath("沈阳","杭州");
    }
}
