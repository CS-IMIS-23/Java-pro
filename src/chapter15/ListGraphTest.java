package chapter15;

import java.util.Iterator;

public class ListGraphTest
{
    public static void main(String[] args) {
        ListGraph<Integer> listGraph = new ListGraph<>();

        listGraph.addVertex(2);
        listGraph.addVertex(8);
        listGraph.addVertex(9);
        listGraph.addVertex(1);
        listGraph.addVertex(0);


        for (int i=0;i<listGraph.size();i++)
            listGraph.addEdge(i,i+1);
        //listGraph.removeEdge(2,3);

        listGraph.addEdge(0,4);


        System.out.println(listGraph);

//       listGraph.removeVertex(1);
//        System.out.println(listGraph);

        Iterator iterator = listGraph.iteratorDFS(9);
        while (iterator.hasNext())
            System.out.println(iterator.next());

        System.out.println(listGraph.shortestPathLength(0,3));


    }
}
