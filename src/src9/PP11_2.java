package src9;

import java.util.Scanner;

public class PP11_2
{
    public static void main(String[] args) {
        String obj="";
        StringTooLongException problem = new StringTooLongException("The message is over 20 characters.");
        Scanner scan = new Scanner(System.in);

        while (!obj.equals("stop"))
        {
            System.out.println("Please input a message: (stop to quit)");
            obj=scan.nextLine();
            try
            {
                if(obj.length()>20)
                    throw problem;
            }
            catch (StringTooLongException e) {
                System.out.println(problem+obj);
                e.printStackTrace();
            }
        }
        }
    }

