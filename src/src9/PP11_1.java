package src9;
import java.util.Scanner;

public class PP11_1
{
    public static void main(String[] args)throws StringTooLongException{
        String obj;
        StringTooLongException problem = new StringTooLongException("The message is over 20 characters.");
        Scanner scan = new Scanner(System.in);

        System.out.print("Please input a message: ");
        obj=scan.nextLine();
        if (obj.length()>20)
            throw problem;

        System.out.println("End of main method.");
    }
}
