package src9;

public class PP12_9 {
    public static void main(String[] args) {
          int n = 7,x=1;
          int[][] pascal = new int[n][2*n-1];
          while (x<=n)
          {
              int[] test = Pascal(x);
              int c =0;
              int k = n-x;
              while (c+1<=x)
              {
                  pascal[x-1][k] = test[c];
                  c++;
                  k+=2;
              }
              for (int number:pascal[x-1]) {
                  if(number==0)
                      System.out.print(" ");
                  else
                      System.out.print(number);
                  }
                  System.out.println();
              x++;
          }
    }
    public static int[] Pascal(int n) {
        int[] pascal = new int[n];
        int index = 1;
        if (n <= 1) {
            if (n == 1)
                pascal[0] = 1;
        }
        else
            {
                pascal[0] = 1;
                pascal[n-1] = 1;
                while (index<n-1)
                {
                    pascal[index]=Pascal(n-1)[index-1]+Pascal(n-1)[index];
                    index++;
                }
            }
        return pascal;
    }
}


