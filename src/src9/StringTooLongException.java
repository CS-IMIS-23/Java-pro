package src9;

public class StringTooLongException extends Exception
{
    public StringTooLongException(String message)
    {
        super(message);
    }
}


