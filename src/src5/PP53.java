
import java.util.Scanner;

public class PP53
{
  public static void main(String[] args)
  {
     String figure;
     char character;
     int left = 0,number;
     int count1=0,count2=0,count3=0;

     Scanner scan = new Scanner(System.in);

     System.out.print("Enter a figure: ");
     figure = scan.nextLine();
     
     number = figure.length();

     while (left<number)
     {
       character = figure.charAt(left);
       String str = String.valueOf(character);
       int num = Integer.valueOf(str);
       
       if (num == 0)
          count1++;
       else 
         if (num%2==0)
       	  count2++;
	     else
	      count3++;
	
	left++;
     }

     System.out.println("The number of '0' : "+count1);
     System.out.println("The number of odd number : "+count3);
     System.out.println("The number of even number : "+(count1+count2));
   }
}
