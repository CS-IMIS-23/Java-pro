public class PP66
{
   public static void main(String[] args)
   {  
      int count=0;

      Coin myCoin = new Coin();
  
      for(int num=1;num<=100;num++)
      {
          myCoin.flip();
	  
	  if (myCoin.isHeads())
	     count++;
      }
      
      System.out.println("The time of Head: "+count);
   }
}
