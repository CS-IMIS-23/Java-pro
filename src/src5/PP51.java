import java.util.Scanner;

public class PP51
{
   public static void main(String[] args)
   {
      int year;
      int num1,num2,num3;

      Scanner scan = new Scanner(System.in);

      System.out.print("The year is : ");
      year = scan.nextInt();

      while (year<1582)
      {
         System.out.println("Invalid input. Please reenter.");
	 year = scan.nextInt();
      }

      num1 = year%4;
      num2 = year%100;
      num3 = year%400;
       
     if ((num1==0&&num2!=0)||(num2==0&&num3==0))
         System.out.println("The year is a leap year.");
     else
         System.out.println("The year is NOT a leap year.");
    }
}

