package chapter9;

public class PP94Test
{
    public static void main(String[] args) {
        Integer[] list = {7843, 4568, 8765, 6543, 7865,1211, 9987, 3241,
                6589, 6622,4532};

        long a=System.currentTimeMillis();
        Sorting.quickSort2(list);
        System.out.println("旧版本执行耗时 : "+(System.currentTimeMillis()-a)+" 毫秒 ");

        long b=System.currentTimeMillis();
        Sorting.quickSort2(list);
        System.out.println("新版本执行耗时 : "+(System.currentTimeMillis()-b)+" 毫秒 ");

        for (int num:list)
            System.out.print(num+" ");


    }
}
