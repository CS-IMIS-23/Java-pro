package chapter9;

public class Sorting
{
    public static <T extends Comparable<T>> void selectionSort(T[] data)
    {
        long startTime=System.nanoTime(); //获取开始时间
        int min;
        T temp;
        int totalTimes = 0;
        for (int index = 0; index < data.length-1; index++)
        {
            totalTimes++;
            min = index;
            for (int scan = index+1; scan < data.length; scan++)
            {
                totalTimes++;
                if (data[scan].compareTo(data[min])<0)
                    min = scan;
            }

            swap(data, min, index);
        }
        System.out.println("选择排序程序运行时间： "+(System.nanoTime()-startTime)+"ns");
        System.out.println("选择排序总比较次数："+totalTimes);
    }


    private static <T extends Comparable<T>> void swap(T[] data, int index1, int index2)
    {
        T temp = data[index1];
        data[index1] = data[index2];
        data[index2] = temp;
    }

    public static <T extends Comparable<T>> void insertionSort(T[] data)
    {
        long startTime=System.nanoTime(); //获取开始时间
        int totalTimes = 0;
        for (int index = 1; index < data.length; index++)
        {
            totalTimes++;
            T key = data[index];
            int position = index;

            // shift larger values to the right
            while (position > 0 && data[position-1].compareTo(key) > 0)
            {
                data[position] = data[position-1];
                position--;
                totalTimes++;
            }

            data[position] = key;
        }
        System.out.println("插入排序程序运行时间： "+(System.nanoTime()-startTime)+"ns");
        System.out.println("插入排序总比较次数："+totalTimes);
    }

    public static <T extends Comparable<T>> void bubbleSort(T[] data)
    {
        long startTime=System.nanoTime(); //获取开始时间
        int totalTimes = 0;
        int position, scan;
        T temp;
        for (position =  data.length - 1; position >= 0; position--)
        {
            totalTimes++;
            for (scan = 0; scan <= position - 1; scan++)
            {
                totalTimes++;
                if (data[scan].compareTo(data[scan+1]) > 0)
                    swap(data, scan, scan + 1);
            }
        }
        System.out.println("冒泡排序程序运行时间： "+(System.nanoTime()-startTime)+"ns");
        System.out.println("冒泡排序总比较次数："+totalTimes);
    }
    public static <T extends Comparable<T>>void gapSort(T[] data,int i)
    {

        int scan;
        while (i>=1)
        {
            for (scan = 0;scan<=data.length-1-i;scan++)
            {
                if (data[scan].compareTo(data[scan + i]) > 0)
                    swap(data, scan, scan + i);
            }
            i -=2;
        }
    }


    private static int times;
    public static <T extends Comparable<T>> void mergeSort(T[] data)
    {
        int totalTimes = 0;
        long startTime=System.nanoTime(); //获取开始时间
        mergeSort(data, 0, data.length - 1);
        System.out.println("归并排序程序运行时间： "+(System.nanoTime()-startTime)+"ns");

        System.out.println("归并排序总比较次数："+times);
    }


    private static <T extends Comparable<T>>
    void mergeSort(T[] data, int min, int max)
    {
        if (min < max)
        {
            int mid = (min + max) / 2;
            mergeSort(data, min, mid);
            mergeSort(data, mid+1, max);
            merge(data, min, mid, max);
        }
        times++;
    }

    @SuppressWarnings("unchecked")
    private static <T extends Comparable<T>>
    void merge(T[] data, int first, int mid, int last)
    {
        T[] temp = (T[])(new Comparable[data.length]);

        int first1 = first, last1 = mid;  // endpoints of first subarray
        int first2 = mid+1, last2 = last;  // endpoints of second subarray
        int index = first1;  // next index open in temp array

        //  Copy smaller item from each subarray into temp until one
        //  of the subarrays is exhausted
        while (first1 <= last1 && first2 <= last2)
        {
            if (data[first1].compareTo(data[first2]) < 0)
            {
                temp[index] = data[first1];
                first1++;
            }
            else
            {
                temp[index] = data[first2];
                first2++;
            }
            index++;
        }

        //  Copy remaining elements from first subarray, if any
        while (first1 <= last1)
        {
            temp[index] = data[first1];
            first1++;
            index++;
        }

        //  Copy remaining elements from second subarray, if any
        while (first2 <= last2)
        {
            temp[index] = data[first2];
            first2++;
            index++;
        }

        //  Copy merged data into original array
        for (index = first; index <= last; index++)
            data[index] = temp[index];
    }

    private static int count = 0;
    public static <T extends Comparable<T>> void
    quickSort(T[] data)
    {
        long startTime=System.nanoTime(); //获取开始时间
        quickSort(data, 0, data.length - 1);
        System.out.println("快速排序程序运行时间： "+(System.nanoTime()-startTime)+"ns");
        System.out.println("快速排序总比较次数"+count);
    }

    private static <T extends Comparable<T>>
    void quickSort(T[] data, int min, int max)
    {

        if (min < max)
        {
            // create partitions
            int indexofpartition = partition(data, min, max);

            // sort the left partition (lower values)
            quickSort(data, min, indexofpartition - 1);

            // sort the right partition (higher values)
            quickSort(data, indexofpartition + 1, max);

        }
        count++;
    }

    private static <T extends Comparable<T>>
    int partition(T[] data, int min, int max)
    {
        T partitionelement;
        int left, right;
        int middle = (min + max) / 2;

        // use the middle data value as the partition element
        partitionelement = data[middle];
        // move it out of the way for now
        swap(data, middle, min);

        left = min;
        right = max;

        while (left < right)
        {
            // search for an element that is > the partition element
            while (left < right && data[left].compareTo(partitionelement) <= 0)
                left++;

            // search for an element that is < the partition element
            while (data[right].compareTo(partitionelement) > 0)
                right--;

            // swap the elements
            if (left < right)
                swap(data, left, right);
        }

        // move the partition element into place
        swap(data, min, right);

        return right;
    }
    public static <T extends Comparable<T>> void
    quickSort2(T[] data)
    {
        quickSort2(data, 0, data.length - 1);
    }


    private static <T extends Comparable<T>>
    void quickSort2(T[] data, int min, int max)
    {
        if (min < max)
        {
            // create partitions
            int indexofpartition = partition2(data, min, max);

            // sort the left partition (lower values)
            quickSort2(data, min, indexofpartition - 1);

            // sort the right partition (higher values)
            quickSort2(data, indexofpartition + 1, max);
        }
    }

    private static <T extends Comparable<T>>
    int partition2(T[] data, int min, int max)
    {
        T partitionelement;
        int left, right;
        int middle = (min + max) / 2;


        if (data[min].compareTo(data[middle])>=0)
        {
            if(data[min].compareTo(data[max])<=0)
                partitionelement = data[min];
            else
                if (data[middle].compareTo(data[max])>=0)
                {
                    partitionelement = data[middle];
                    swap(data, middle, min);
                }
                else
                {
                    partitionelement = data[max];
                    swap(data,min,max);
                }
        }
        else
        {
            if(data[min].compareTo(data[max])>=0)
                partitionelement = data[min];
            else
            if (data[middle].compareTo(data[max])<=0)
            {
                partitionelement = data[middle];
                swap(data, middle, min);
            }
            else
            {
                partitionelement = data[max];
                swap(data,min,max);
            }
        }
        left = min;
        right = max;

        while (left < right)
        {
            // search for an element that is > the partition element
            while (left < right && data[left].compareTo(partitionelement) <= 0)
                left++;

            // search for an element that is < the partition element
            while (data[right].compareTo(partitionelement) > 0)
                right--;

            // swap the elements
            if (left < right)
                swap(data, left, right);
        }

        // move the partition element into place
        swap(data, min, right);

        return right;
    }
}
