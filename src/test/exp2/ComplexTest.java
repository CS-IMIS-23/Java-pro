package test.exp2;

import junit.framework.TestCase;
import org.junit.Test;
import static org.junit.Assert.*;
import exp2.Complex;

import java.util.concurrent.ExecutionException;

/** 
* Complex Tester. 
* 
* @author <Authors name> 
* @since <pre>���� 21, 2018</pre> 
* @version 1.0 
*/

public class ComplexTest
{
    Complex a = new Complex(1,2);
    Complex b = new Complex(1,-2);
    Complex c = new Complex(2,3);
    Complex d = new Complex(2,1);
    Complex e = new Complex(2,2);
    Complex f = new Complex(-1,1);

    @Test
    public void testComplexAdd() throws Exception
    {
        assertEquals(3.0+"+5.0i",String.valueOf(a.ComplexAdd(c)));
    }

    @Test
    public void testComplexSub() throws Exception
    {
        assertEquals(2.0+"-3.0i",String.valueOf(b.ComplexSub(f)));
    }

    @Test
    public void testComplexMulti() throws Exception
    {
        assertEquals(1.0+"+8.0i",String.valueOf(c.ComplexMulti(d)));
    }

    @Test
    public void testComplexDiv() throws Exception
    {
        assertEquals(0.75+"-0.25i",String.valueOf(d.ComplexDiv(e)));
    }
}

