package test.exp3;

import exp3.Complex;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ComplexTest {
    Complex a = new Complex(1,2);
    Complex b = new Complex(1,-2);
    Complex c = new Complex(2,3);
    Complex d = new Complex(2,1);
    Complex e = new Complex(2,2);
    Complex f = new Complex(-1,1);
    Complex g = new Complex(1,-1);
    Complex h = new Complex(1,1);

    @Test
    public void testComplexAdd() throws Exception
    {
        assertEquals("(3.0+5.0i)",String.valueOf(a.add(c)));
    }

    @Test
    public void testComplexSub() throws Exception
    {
        assertEquals("(2.0-3.0i)",String.valueOf(b.minus(f)));
    }

    @Test
    public void testComplexMulti() throws Exception
    {
        assertEquals("(1.0+8.0i)",String.valueOf(c.multiply(d)));
    }

    @Test
    public void testComplexDiv() throws Exception
    {
        assertEquals("(0.0+1.0i)",String.valueOf(g.divide(h)));
    }
} 
