package test.src7;

import junit.framework.TestCase;
import org.junit.Test;
import src7.MyUtil;


public class MyUtilTest extends TestCase {
    @Test
    public void testNormal() {
        assertEquals("failed", MyUtil.percentage2fivegrade(48));
        assertEquals("pass", MyUtil.percentage2fivegrade(68));
        assertEquals("medium", MyUtil.percentage2fivegrade(78));
        assertEquals("good", MyUtil.percentage2fivegrade(88));
        assertEquals("excellent", MyUtil.percentage2fivegrade(98));
    }


    @Test
    public void testException(){
            assertEquals("error", MyUtil.percentage2fivegrade(-50));
            assertEquals("error",MyUtil.percentage2fivegrade(190));
        }


    @Test
        public void testBoundary(){
        assertEquals("failed",MyUtil.percentage2fivegrade(0));
        assertEquals("pass",MyUtil.percentage2fivegrade(60));
        assertEquals("medium",MyUtil.percentage2fivegrade(70));
        assertEquals("good",MyUtil.percentage2fivegrade(80));
        assertEquals("excellent",MyUtil.percentage2fivegrade(90));
        assertEquals("excellent",MyUtil.percentage2fivegrade(100));
    }
}