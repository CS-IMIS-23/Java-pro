package test.src7;

import junit.framework.TestCase;
import org.junit.Test;
import src7.StringBufferDemo;

public class StringBufferDemoTest extends TestCase{
          StringBuffer a = new StringBuffer("asdfghjklqwe");
          StringBuffer b = new StringBuffer("asdfghjklqwerrttttyytuio");
          StringBuffer c = new StringBuffer("ghgnvmxnifaijhewhfjkjkjgafgakjjgkjkg");

      @Test
      public void testcharAt() throws Exception
      {
         assertEquals('a',a.charAt(0));
         assertEquals('a',b.charAt(0));
         assertEquals('g',c.charAt(0));
      }
      @Test
    public void testcapacity() throws Exception{
          assertEquals(28,a.capacity());
          assertEquals(40,b.capacity());
          assertEquals(52,c.capacity());
   }

   @Test
   public void testlength() throws Exception{
          assertEquals(12,a.length());
          assertEquals(24,b.length());
          assertEquals(36,c.length());
   }

   @Test
    public void testindexOf() throws Exception{
          assertEquals(0,a.indexOf("asd"));
          assertEquals(5,b.indexOf("hjk"));
          assertEquals(6,c.indexOf("xni"));
   }

}


