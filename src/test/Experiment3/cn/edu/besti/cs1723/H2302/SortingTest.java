package test.Experiment3.cn.edu.besti.cs1723.H2302;

import Experiment3.cn.edu.besti.cs1723.H2302.Sorting;
import junit.framework.TestCase;
import org.junit.Test;


public class SortingTest extends TestCase
{

    @Test
    public void testNormal() throws Exception
    {
        Integer[] integers1 = {2,7,12,13,14,23,29,32,46,56};
        Integer[] integers2 = {7,32,12,46,14,56,29,13,23,2};
        Sorting.shellSort(integers2);
        for (int i=0;i<integers2.length;i++)
            assertEquals(integers1[i],integers2[i]);

        Integer[] integers3 = {2,7,12,13,14,23,29,32,46,53};
        Integer[] integers4 = {7,32,12,46,14,53,29,13,23,2};
        Sorting.heapSort(integers4);
        for (int i=0;i<integers4.length;i++)
            assertEquals(integers3[i],integers4[i]);

        Integer[] integers5 = {2,7,12,13,17,23,29,32,46,53};
        Integer[] integers6 = {7,32,12,46,17,53,29,13,23,2};
        Sorting.binaryTreeSort(integers6);
        for (int i=0;i<integers6.length;i++)
            assertEquals(integers5[i],integers6[i]);

    }
    @Test
    public void testAbnormal() throws Exception
    {
        Integer[] integers1 = {2,7,12,13,14,23,29,32,46,56};
        Integer[] integers2 = {7,32,12,46,14,56,29,14,23,2};
        Sorting.shellSort(integers2);
        boolean result1 = true;
        for (int i =0 ;i<integers2.length;i++)
        {
            if (integers1[i]!=integers2[i])
            {
                result1 = false;
                break;
            }
        }
        assertEquals(false,result1);

        Integer[] integers3 = {2,7,12,13,14,23,29,32,46,56};
        Integer[] integers4 = {7,32,19,46,14,56,29,13,23,2};
        Sorting.heapSort(integers4);
        boolean result2 = true;
        for (int i =0 ;i<integers4.length;i++)
        {
            if (integers3[i]!=integers4[i])
            {
                result2 = false;
                break;
            }
        }
        assertEquals(false,result2);

        Integer[] integers5 = {2,7,12,13,14,23,24,32,46,56};
        Integer[] integers6 = {7,32,12,46,14,56,29,13,23,2};
        Sorting.binaryTreeSort(integers5);
        boolean result3 = true;
        for (int i =0 ;i<integers5.length;i++)
        {
            if (!integers5[i].equals(integers6[i]))
            {
                result3 = false;
                break;
            }
        }
        assertEquals(false,result3);

    }
    @Test
    public void testBoundary () throws Exception{
        Integer[] integers1 = {2,7,12,13,14,23,29,32,46,780};
        Integer[] integers2 = {7,32,12,46,14,780,29,13,23,2};
        Sorting.shellSort(integers2);
        boolean result1 = true;
        for (int i =0 ;i<integers2.length;i++)
        {
            if (!integers1[i].equals(integers2[i]))
            {
                result1 = false;
                break;
            }
        }
        assertEquals(true,result1);

        Integer[] integers3 = {-139409,7,12,13,14,23,29,32,46,56};
        Integer[] integers4 = {7,32,19,46,14,56,29,13,23,-139409};
        Sorting.binaryTreeSort(integers4);
        boolean result2 = true;
        for (int i =0 ;i<integers4.length;i++)
        {
            if (integers3[i]!=integers4[i])
            {
                result2 = false;
                break;
            }
        }
        assertEquals(false,result2);

        Integer[] integers5 = {2,7,12,13,14,23,29,32,46,780};
        Integer[] integers6 = {7,32,12,46,14,780,29,13,23,2};
        Sorting.shellSort(integers6);
        boolean result3 = true;
        for (int i =0 ;i<integers6.length;i++)
        {
            if (!integers5[i].equals(integers6[i]))
            {
                result1 = false;
                break;
            }
        }
        assertEquals(true,result3);
    }
}
