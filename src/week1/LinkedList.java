package week1;

public class LinkedList
{
    public static void main(String[] args) {
        Student student = new Student("GK",2301,"Learning");
        Student Head = student;

        Student node = new Student("Hzy",2302,"Football");
        Student student1 = new Student("Fwq",2303,"English");
        Student student2 = new Student("Dzx",2304,"WanZhe");
        Student student3 = new Student("TX",2305,"Java");
        Student student4 = new Student("LC",2306,"eat");
        Student student5 = new Student("PL",2312,"game");
        Student student6 = new Student("HYT",2307,"game");
        InsertNode(Head,node);
        InsertNode(Head,student1);
        InsertNode(Head,student2);
        InsertNode(Head,student3);
        InsertNode(Head,student4);
        InsertNode(Head,student5);
        InsertNode(Head,student4,student5,student6);
        PrintLinkedList(Head);

        DeleteNode(Head,student4);
        PrintLinkedList(Head);
    }
    public static void PrintLinkedList(Student Head){
        Student node = Head;
        while (node !=null)
        {
            System.out.println("姓名: "+node.name +" 学号："+node.number+" 爱好："+node.hobby);
            node = node.next;
        }
    }

    public static void InsertNode(Student Head,Student node)
    {
        Student node1 = Head;
        while (node1.next!=null)
        {
            node1 = node1.next;
        }
        node1.next = node;
    }
    public static void InsertNode(Student Head,Student node1,Student node2,Student node)
    {
        Student point = Head;
        while (point.next.number!=node2.number && point !=null)
        {
            point = point.next;
        }
        if(point.number == node1.number)
        {
            node.next = point.next;
            point.next = node;
        }
        else
            System.out.println("Error");
    }
    public static void DeleteNode(Student Head,Student node)
    {
        Student PreNode = Head,CurrentNode = Head;
        while (CurrentNode!=null)
        {
            if(CurrentNode.number!=node.number)
            {
                PreNode = CurrentNode;
                CurrentNode = CurrentNode.next;
            }
            else
                break;
        }
        PreNode.next =CurrentNode.next;
    }
}
