package week1;

import jsjf.ArrayStack;


import java.util.*;


public class PP32
{
    public static void main(String[] args) {
        String message;
        Scanner scan = new Scanner(System.in);

        System.out.println("Enter a line of text: ");
        message = scan.nextLine();

        StringTokenizer stringTokenizer = new StringTokenizer(message);
        String word,result="";


        while (stringTokenizer.hasMoreTokens())
        {
            word = stringTokenizer.nextToken();
            ArrayStack arrayStack = new ArrayStack();
            for (int i=0; i<word.length();i++)
                arrayStack.push(word.charAt(i));

            while (!arrayStack.isEmpty())
            {
                result +=arrayStack.pop();
            }
            result += " ";
        }
        System.out.println("反向输出："+result);
    }
}

