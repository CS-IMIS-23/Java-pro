package week1;

public class IntegerLinkedList
{
    public static void main(String[] args) {
        Integer integer = new Integer(6);
        Integer Head = integer;

        Integer integer1 = new Integer(1);
        Integer integer2 = new Integer(7);
        Integer integer3 = new Integer(9);
        Integer integer4 = new Integer(4);

        InsertNode(Head,integer1);
        InsertNode(Head,integer2);
        InsertNode(Head,integer3);

        System.out.print("初始链表打印:");
        PrintLinkedList(Head);

        System.out.print("在1和7之间插入数字9: ");
        InsertNode(Head,integer1,integer2,integer4);
        PrintLinkedList(Head);

        System.out.print("删除数字7后: ");
        DeleteNode(Head,integer2);
        PrintLinkedList(Head);

        Sorting(Head);
        System.out.print("排序后: ");
        PrintLinkedList(Head);

    }
    public static void PrintLinkedList(Integer Head){
        Integer node = Head;
        while (node !=null)
        {
            System.out.print(node.integer+" ");
            node = node.next;
        }
        System.out.println();
    }

    public static void InsertNode(Integer Head,Integer node)
    {
        Integer node1 = Head;
        while (node1.next!=null)
        {
            node1 = node1.next;
        }
        node1.next = node;
    }
    public static void InsertNode(Integer Head,Integer node1,Integer node2,Integer node)
    {
        Integer point = Head;
        while (point.next.integer!=node2.integer && point !=null)
        {
            point = point.next;
        }
        if(point.integer == node1.integer)
        {
            node.next = point.next;
            point.next = node;
        }
        else
            System.out.println("Error");
    }
    public static void DeleteNode(Integer Head,Integer node)
    {
        Integer PreNode = Head,CurrentNode = Head;
        while (CurrentNode!=null)
        {
            if(CurrentNode.integer!=node.integer)
            {
                PreNode = CurrentNode;
                CurrentNode = CurrentNode.next;
            }
            else
                break;
        }
        PreNode.next =CurrentNode.next;
    }
    public static void Sorting(Integer Head) {
        Integer min;
        int temp;
        Integer numNode = Head;
        while (numNode != null) {
            min = numNode;
            Integer current = min.next;
            while (current != null) {
                if (current.integer > min.integer)
                    min = current;
                current = current.next;
            }
            temp = min.integer;
            min.integer = numNode.integer;
            numNode.integer = temp;

            numNode = numNode.next;
        }
    }
}
