package week1;

public class Student
{
    protected String name;
    protected int number;
    protected String hobby;

    protected Student next = null;

    public Student(String name, int number, String hobby) {
        this.name = name;
        this.number = number;
        this.hobby = hobby;
    }
}
