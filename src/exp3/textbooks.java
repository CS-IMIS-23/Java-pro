package exp3;

public class textbooks
{
    private int pages;
    private String keywords;
    public textbooks(int page,String keyword)
    {
        setPages(page);
        setKeywords(keyword);
    }
    public int getPages()
    {
        return pages;
    }

    public String getKeywords() {
        return keywords;
    }

    @Override
    public String toString() {
        return "textbooks{" +
                "pages=" + pages +
                ", keywords='" + keywords + '\'' +
                '}';
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }
}
