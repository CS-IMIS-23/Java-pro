package exp3

/**
 * @author lenovo
 */
object CodeStandard {
    @JvmStatic
    fun main(args: Array<String>) {
        val number = 20
        val buffer = StringBuffer()
        buffer.append('S')
        buffer.append("tringBuffer")
        println(buffer[1])
        println(buffer.capacity())
        println(buffer.indexOf("tring"))
        println("buffer = " + buffer.toString())
        if (buffer.capacity() < number) {
            buffer.append("1234567")
        }
        for (i in 0 until buffer.length) {
            println(buffer[i])
        }
    }
}

