package practice;

import src8.Sorting3;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.StringTokenizer;

/**
 * Created by besti on 2018/6/9.
 */
public class SocketServer {
    public static void main(String[] args) throws IOException {
        //1.建立一个服务器Socket(ServerSocket)绑定指定端口
        ServerSocket serverSocket=new ServerSocket(8800);
        //2.使用accept()方法阻止等待监听，获得新连接
        Socket socket=serverSocket.accept();
        //3.获得输入流
        InputStream inputStream=socket.getInputStream();
        BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(inputStream,"UTF-8"));
        //获得输出流
        OutputStream outputStream=socket.getOutputStream();
        PrintWriter printWriter=new PrintWriter(outputStream);
        //4.读取用户输入信息
        String info=null;
        info = bufferedReader.readLine();
        System.out.println("我是20172302侯泽洋，用户20172308周亚杰发送信息为：" + info);

        String info0 = info;
        StringTokenizer answer = new StringTokenizer(info0);
        int[] array = new int[7];
        int x=0;
        while (answer.hasMoreTokens())
        {
            array[x] = Integer.parseInt(answer.nextToken());
            x++;
        }
        Sorting3.selectionSort(array);
        String reply="";
        for (int i=0;i<array.length;i++)
            reply +=array[i]+ " ";

        //给客户一个响应
        printWriter.write(reply);
        printWriter.flush();
        //5.关闭资源
        printWriter.close();
        outputStream.close();
        bufferedReader.close();
        inputStream.close();
        socket.close();
        serverSocket.close();
    }
}
