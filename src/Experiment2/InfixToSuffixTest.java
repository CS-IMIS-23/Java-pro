package Experiment2;

import chapter10.PostfixEvaluator;

public class InfixToSuffixTest
{
    public static void main(String[] args) {
        InfixToSuffix infixToSuffix = new InfixToSuffix();
        String x = infixToSuffix.infixToSuffix("4 * 2 - 3 / 1 + 1");

        System.out.println("后缀表达式为： "+x);
        PostfixEvaluator evaluator = new PostfixEvaluator();
        int result =  evaluator.evaluate(x);
        System.out.println("计算结果为："+result);
    }

}
