package Experiment2;

import java.util.Iterator;
import java.util.Stack;
import java.util.StringTokenizer;

public class InfixToSuffix
{
    private Stack<LinkedBinaryTree> linkedBinaryTreeStack = new Stack<LinkedBinaryTree>();
    private Stack stack = new Stack();
    private int value;



    public InfixToSuffix()
    {
        value = 0;
    }


    public String infixToSuffix(String s)
    {
        String result="";
        StringTokenizer stringTokenizer = new StringTokenizer(s);

        while (stringTokenizer.hasMoreTokens())
        {
            String m = stringTokenizer.nextToken();
            if (isOperator(m)) {

                if (m.equals("*") || m.equals("/"))
                    stack.push(m);
                else if (stack.empty())
                    stack.push(m);
                else {
                        while (!stack.isEmpty()) {
                            String s1 = String.valueOf(stack.pop());
                            LinkedBinaryTree operand3 = linkedBinaryTreeStack.pop();
                            LinkedBinaryTree operand4 = linkedBinaryTreeStack.pop();
                            LinkedBinaryTree<String> linkedBinaryTree1 = new LinkedBinaryTree<String>(s1, operand4, operand3);
                            linkedBinaryTreeStack.push(linkedBinaryTree1);
                            if (stack.isEmpty())
                                break;
                    }
                    stack.push(m);
                }
            }


            else
            {
                LinkedBinaryTree<String> linkedBinaryTree3 = new LinkedBinaryTree<String>(m);
                linkedBinaryTreeStack.push(linkedBinaryTree3);
            }
        }
        value = stack.size();
        for (int y = 0; y < value; y++)
        {
            String s2  = String.valueOf(stack.pop());
            LinkedBinaryTree operand5 = linkedBinaryTreeStack.pop();
            LinkedBinaryTree operand6 = linkedBinaryTreeStack.pop();
            LinkedBinaryTree<String> linkedBinaryTree1 = new LinkedBinaryTree<String>(s2,operand6,operand5);
            linkedBinaryTreeStack.push(linkedBinaryTree1);
        }

        LinkedBinaryTree linkedBinaryTree =  linkedBinaryTreeStack.pop();

        System.out.println("这棵树为："+linkedBinaryTree.toString());
        Iterator iterator = linkedBinaryTree.iteratorPostOrder();
        while (iterator.hasNext())
            result += iterator.next()+ " ";

        return result;
    }
    private boolean isOperator(String token) {
        return (token.equals("+") || token.equals("-") || token.equals("*") || token
                .equals("/"));
    }

}
