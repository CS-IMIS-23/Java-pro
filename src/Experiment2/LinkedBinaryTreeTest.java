package Experiment2;

import java.util.Iterator;

public class LinkedBinaryTreeTest
{
    public static void main(String[] args) {
        LinkedBinaryTree<Integer> linkedBinaryTree1 = new LinkedBinaryTree<Integer>(3);
        LinkedBinaryTree<Integer> linkedBinaryTree2 = new LinkedBinaryTree<Integer>(1);
        LinkedBinaryTree<Integer> linkedBinaryTree3 = new LinkedBinaryTree<Integer>(2,linkedBinaryTree1,linkedBinaryTree2);
        LinkedBinaryTree<Integer> linkedBinaryTree4 = new LinkedBinaryTree<Integer>(5);
        LinkedBinaryTree<Integer> linkedBinaryTree5 = new LinkedBinaryTree<Integer>(4,linkedBinaryTree4,linkedBinaryTree4);
        LinkedBinaryTree<Integer> linkedBinaryTree6 = new LinkedBinaryTree<Integer>(6,linkedBinaryTree3,linkedBinaryTree5);

        System.out.println("打印这棵树"+linkedBinaryTree6);
        System.out.println("显示这棵树的右子树 "+linkedBinaryTree6.getRight().toString());
        System.out.println("是否包含元素3： "+linkedBinaryTree6.contains(3));

        System.out.println("先序遍历：");
        Iterator iterator1 = linkedBinaryTree6.iteratorPreOrder();
        while (iterator1.hasNext())
            System.out.print(iterator1.next()+" ");

        System.out.println();
        System.out.println("后序遍历：");
        Iterator iterator2 = linkedBinaryTree6.iteratorPostOrder();
        while (iterator2.hasNext())
            System.out.print(iterator2.next()+" ");
    }
}
