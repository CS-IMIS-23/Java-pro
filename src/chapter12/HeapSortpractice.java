package chapter12;

public class HeapSortpractice
{
    public static void main(String[] args) {
        int[] array ={36,30,18,40,32,45,22,50};
        LargeArrayHeap<Integer> largeArrayHeap = new LargeArrayHeap();
        for (int i = 0;i<array.length;i++)
        {
            largeArrayHeap.addElement(array[i]);
        }

        System.out.println("大顶堆序列（层序遍历）");
        System.out.println(largeArrayHeap);

        System.out.println("每轮排序中数组的结果");
        for (int i = 0;i<array.length;i++)
        {
            array[i] = largeArrayHeap.removeMax();
            System.out.println(largeArrayHeap);
        }

        System.out.println("降序排列，排序结果为：");
        for (int i= 0;i<array.length;i++)
            System.out.print(array[i]+" ");
    }
}
