package chapter12;

import chapter12.jsjf.LinkedHeap;

public class Test
{
    public static void main(String[] args) {
        HeapQueue<Integer> heapQueue = new HeapQueue<Integer>();
        heapQueue.enqueue(2);
        heapQueue.enqueue(3);
        heapQueue.enqueue(4);
        heapQueue.enqueue(5);


        System.out.println("打印队列元素："+heapQueue.toString());
        System.out.println("删除第一元素");
        heapQueue.dequeue();
        System.out.println();
        System.out.println(heapQueue.toString());

//        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>();
//        priorityQueue.addElement(1,1);
//        priorityQueue.addElement(2,3);
//        priorityQueue.addElement(9,3);
//        priorityQueue.addElement(1,2);
//        priorityQueue.addElement(4,0);
//        System.out.println(priorityQueue);
//
//        priorityQueue.removeNext();
//        System.out.println(priorityQueue);
//
//        LinkedHeap<Integer> linkedHeap =new LinkedHeap<Integer>() ;
//        linkedHeap.addElement(78);
//        linkedHeap.addElement(26);
//        linkedHeap.addElement(98);
//        linkedHeap.addElement(34);
//        System.out.println(linkedHeap);
//
//        linkedHeap.removeMin();
//        System.out.println(linkedHeap);
    }
}
