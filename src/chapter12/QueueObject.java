package chapter12;

public class QueueObject<T> implements Comparable<QueueObject>
{
    private static int nextOrder = 0;
    private int arrivalOrder;
    private T element;

    public QueueObject(T element)
    {
        this.element = element;
        arrivalOrder = nextOrder;
        nextOrder++;
    }




    @Override
    public int compareTo(QueueObject o) {

        if (arrivalOrder>o.getArrivalOrder())
            return 1;
        else
            return -1;
    }

    public int getArrivalOrder() {
        return arrivalOrder;
    }

    public T getElement() {
        return element;
    }

    @Override
    public String toString() {
        String result="";
        result = element + "  ";
        return result;
    }
}
