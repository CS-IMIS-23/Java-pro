package chapter12;

import chapter12.jsjf.ArrayHeap;

public class HeapQueue<T> extends ArrayHeap<QueueObject<T>>
{
    public HeapQueue()
    {
        super();
    }

    public void enqueue(T t)
    {
        QueueObject<T> queueObject = new QueueObject<T>(t);
        super.addElement(queueObject);
    }
    public T dequeue()
    {
        QueueObject<T> queueObject;
        queueObject = (QueueObject<T>) super.removeMin();

        return queueObject.getElement();
    }

    public T first()
    {
        QueueObject<T> queueObject = (QueueObject<T>) super.findMin();
        return  queueObject.getElement();
    }
    public boolean isEmpty()
    {
        return super.isEmpty();
    }
    public int size()
    {
        return  super.size();
    }
    public String toString()
    {
        String result = "";
        int x = size();
        for(int i=0;i<x;i++)
        {
            T t = dequeue();
            result += t+" ";
            enqueue(t);
        }

        return  result;
    }

}
