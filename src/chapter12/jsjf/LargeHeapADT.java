package chapter12.jsjf;

import chapter10.jsjf.BinaryTreeADT;

public interface LargeHeapADT<T> extends BinaryTreeADT<T>
{
    public void addElement(T obj);

    /**
     * Removes element with the lowest value from this heap.
     *
     * @return the element with the lowest value from the heap
     */
    public T removeMax();

    /**
     * Returns a reference to the element with the lowest value in
     * this heap.
     *
     * @return a reference to the element with the lowest value in the heap
     */
    public T findMax();
}
