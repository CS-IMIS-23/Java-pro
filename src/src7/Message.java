package src7;

import java.awt.*;

public class Message
{
    public static void main(String[] args) {
        Thought parked = new Thought();
        Advice dates = new Advice();

        parked.message();
        dates.message();
    }
}
