package src7;

public class Dictionary extends Book
{
    private int definitions = 52500;

    //-----------------------------------------------------
    // Prints a message using boeh and inherited values.
    //------------------------------------------------------

    public double computeRatio()
    {
        return (double) definitions/pages;
    }

    //-------------------------------------------------------------
    //   Definitions mutator.
    //--------------------------------------------------------------

    public void setDefinitions(int numDefinitions)
    {
        definitions = numDefinitions;
    }

    public int getDefinitions()
    {
        return definitions;
    }

}
