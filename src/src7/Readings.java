package src7;

public class Readings
{
    protected int pages;
    protected String antisop;

    public Readings(int pages,String antisop)
    {
        this.pages = pages;
        this.antisop = antisop;
    }

    public void setPages(int numpages)
    {
        pages = numpages;
    }
    public void setAntisop(String antisop)
    {
        this.antisop = antisop;
    }

    public int getPages(int numpages)
    {
        pages = numpages;
        return pages;
    }
    public String getAntisop(String antisop)
    {
        this.antisop = antisop;
        return  antisop;
    }

}
