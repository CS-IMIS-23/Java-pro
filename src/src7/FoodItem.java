package src7;

import java.security.PublicKey;

public class FoodItem
{
    final private int CALORIES_PER_GRAM=9;
    private int fatGrams;
    protected int servings;

    public FoodItem(int numFatGrams,int numServings)
    {
        fatGrams = numFatGrams;
        servings = numServings;
    }
    private int Calories()
    {
        return fatGrams* CALORIES_PER_GRAM;
    }
    public int CaloriesPerServing()
    {
        return (Calories()/servings);
    }
}
