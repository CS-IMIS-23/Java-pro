package src7;

public class Dictionary2 extends Book2
{
    private int definitions ;

    public Dictionary2(int numpages, int numDefinitions)
    {
        super(numpages);
        definitions = numDefinitions;
    }

    //-----------------------------------------------------
    // Prints a message using boeh and inherited values.
    //------------------------------------------------------

    public double computeRatio()
    {
        return (double) definitions/pages;
    }

    //-------------------------------------------------------------
    //   Definitions mutator.
    //--------------------------------------------------------------

    public void setDefinitions(int numDefinitions)
    {
        definitions = numDefinitions;
    }

    public int getDefinitions()
    {
        return definitions;
    }
}
