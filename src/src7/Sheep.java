package src7;

public class Sheep extends Animal
{
    public Sheep(String name, int id) {
        super(name, id);
    }

    @Override
    public void eat() {
        System.out.println("I'm eating grass");
    }

    @Override
    public void sleep() {
        System.out.println("I'm sleeping");
    }

    @Override
    public void introduction() {
        System.out.println("I'm "+id+" "+name);
    }
}
