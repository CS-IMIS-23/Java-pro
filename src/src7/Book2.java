package src7;

public class Book2
{
    protected int pages = 1500;

    public  Book2(int numpages)
    {
        pages = numpages;
    }

    //---------------------------------------------------------
    // Pages mutator.
    //----------------------------------------------------------

    public void setPages(int numPages)
    {
        pages = numPages;
    }

    //------------------------------------------------------------
    // Pages accessor.
    //-------------------------------------------------------------

    public int getPages()
    {
        return  pages;
    }

}