package src7;

public class TextBooks extends Readings
{
    private String names;

    public TextBooks(int pages, String antisop,String names)
    {
        super(pages, antisop);
        this.names = names;
    }
    public String getNames()
    {
        return names;
    }
    public void setNames(String names)
    {
        this.names = names;
    }
    public String toString()
    {
        String result;
        result = "Name: "+names+"\tAntisop:"+antisop+"\tPages: "+pages;
        return result;
    }
}
