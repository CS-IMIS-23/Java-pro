package src7;

public class MonetaryCoin extends Coin
{
    int [] facevalue = {1,5,10,20,50,100};
    private int value,num;

    public MonetaryCoin()
    {
        super.flip();

    }
    public int getValue()
    {
        num = (int)(Math.random()*6);
        value = facevalue[num];
        return value;
    }
}
