package src7;

public class Novels extends Readings
{
    private int section;
    private String name;

    public Novels(int pages, String antisop,int section,String name)
    {
        super(pages, antisop);
        this.section = section;
        this.name = name;
    }

    public void setSection(int section)
    {
        this.section = section;
    }

    public int getSection(int section)
    {
        this.section = section;
        return section;
    }

    public String toString()
    {
        String result;
        result = "Name: "+name+"\t";
        result += "Pages: "+pages+"\t";
        result += "Sections: "+section+"\t";
        result +="Antisop: "+antisop;
        return result;
    }
}
