package src7;

public class MyUtil
{
    public static String percentage2fivegrade(int grade){
        if ((grade < 0))
            return "error";
        //If the score is less than 60, it will be "fail".
        //If the score is less than 60, it will be "fail".
        else if (grade < 60)
            return "failed";
            //如果成绩在60与70之间，转成“及格”
        else if (grade < 70)
            return "pass";
            //如果成绩在70与80之间，转成“中等”
        else if (grade < 80)
            return "medium";
            //如果成绩在80与90之间，转成“良好”
        else if (grade < 90)
            return "good";
            //如果成绩在90与100之间，转成“优秀”
        else if (grade <= 100)
            return "excellent";
            //其他，转成“错误”
        else
            return "error";
    }
}
