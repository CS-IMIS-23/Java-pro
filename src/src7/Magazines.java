package src7;

public class Magazines extends Readings
{
    private int articles;
    private String names;

    public Magazines(int pages, String antisop,int numofartices,String names)
    {
        super(pages, antisop);
        articles = numofartices;
        this.names = names;
    }
    public int getArticles()
    {
        return articles;
    }
    public String getNames()
    {
        return names;
    }
    public void setArticles(int num)
    {
        articles = num;
    }
    public void setNames(String name)
    {
        names = name;
    }
    public String toString()
    {
        String result;
        result = "Names: "+names+"\tPages: "+pages+"\tArticles: "+articles+"\tAntisop: "+antisop;
        return result;
    }
}
