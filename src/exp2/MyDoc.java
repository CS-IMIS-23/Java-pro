package exp2;

// Server Classes
     abstract class Data { abstract public void DisplayValue();}

    class Integer extends  Data { int value;Integer() { value=20172302%6; }
        public void DisplayValue() { System.out.println (value); }}


     class Boolean extends  Data { boolean value;Boolean() { value= true; }
         public void DisplayValue() { System.out.println (value); }}


    // Pattern Classes
    abstract class Factory1 { abstract public Data CreateDataObject();}
    class IntergerFactory extends Factory1 { public Data CreateDataObject(){ return new Integer(); }}

     abstract class Factory2 { abstract public Data CreateDataObject();}
     class BooleanFactory extends Factory2 { public Data CreateDataObject(){ return new Boolean(); }}


    //Client classes
    class Document1 { Data pd;Document1(Factory1 pf){ pd = pf.CreateDataObject(); }
        public void DisplayData(){ pd.DisplayValue(); }}

     class Document2 { Data pd;Document2(Factory2 pf){ pd = pf.CreateDataObject(); }
         public void DisplayData(){ pd.DisplayValue(); }}




        //Test class
public class MyDoc {
    static Document1 d;
    static Document2 f;

    public static void main(String[] args)
    {
        d = new Document1(new IntergerFactory());d.DisplayData();

        f = new Document2(new BooleanFactory());f.DisplayData();
    }
}