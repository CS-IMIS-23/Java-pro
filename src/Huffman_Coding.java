import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class Huffman_Coding
{
    public static void main(String[] args) throws IOException {
        File file = new File("D:\\JAVA程序设计\\Java-pro\\src\\file.txt");

        Scanner scan = new Scanner(file);
        String s = scan.nextLine();
        System.out.println(s);
        int[] array = new int[26];
        for (int i = 0; i < array.length; i++) {
            array[i] = 0;
        }
        for (int i = 0; i < s.length(); i++) {
            char x = s.charAt(i);
            array[x - 'a']++;
        }
        System.out.println("打印各字母出现频率：");
        for (int i = 0; i < array.length; i++) {
            System.out.print((char)('a'+i)+":"+(double) array[i] / s.length()+" ");
        }

        HuffmanTreeNode[] huffmanTreeNodes = new HuffmanTreeNode[array.length];
        for (int i = 0; i < array.length; i++) {
            huffmanTreeNodes[i] = new HuffmanTreeNode(array[i], (char) ('a' + i), null, null, null);
        }

        HuffmanTree huffmanTree = new HuffmanTree(huffmanTreeNodes);

        System.out.println();
        System.out.println("构造的哈夫曼树：");
        System.out.println(huffmanTree.toString());

        System.out.println("打印各字母的编码");
        String[] codes = huffmanTree.getEncoding();
        for (int i = 0; i < codes.length; i++) {
            System.out.println((char) ('a' + i) + ":" + codes[i]);
        }

        //进行编码：
        String result = "";
        for (int i = 0; i < s.length(); i++) {
            int x = s.charAt(i) - 'a';
            result += codes[x];
        }
        System.out.println("编码结果："+result);
        //写入文件
        File file1 = new File("D:\\JAVA程序设计\\Java-pro\\src\\file1.txt");
        FileWriter fileWriter = new FileWriter(file1);
        fileWriter.write(result);
        fileWriter.close();

        //从文件读取
        Scanner scan1 = new Scanner(file1);
        String s1 = scan1.nextLine();
        HuffmanTreeNode huffmanTreeNode = huffmanTree.getmRoot();
        //进行解码
        String result2 = "";
        for (int i = 0; i < s1.length(); i++) {
            if (s1.charAt(i) == '0') {
                if (huffmanTreeNode.getLeft() != null) {
                    huffmanTreeNode = huffmanTreeNode.getLeft();
                }
            } else {
                if (s1.charAt(i) == '1') {
                    if (huffmanTreeNode.getRight() != null) {
                        huffmanTreeNode = huffmanTreeNode.getRight();
                    }
                }
            }
            if (huffmanTreeNode.getLeft() == null && huffmanTreeNode.getRight() == null) {
                result2 += huffmanTreeNode.getElement();
                huffmanTreeNode = huffmanTree.getmRoot();
            }
        }
        System.out.println("解码结果："+result2);
        //写入文件
        File file2 = new File("D:\\JAVA程序设计\\Java-pro\\src\\file2.txt");
        FileWriter fileWriter1 = new FileWriter(file1);
        fileWriter1.write(result2);

        // 判断解码后原来是否一致
        System.out.println("编码解码后原来是否一致："+result2.equals(s));

    }
}
