package jsjf;

public class LinkedStackTest
{
    public static void main(String[] args) {
        LinkedStack linkedStack = new LinkedStack();

        linkedStack.push(1);
        linkedStack.push(2);
        linkedStack.push(3);
        linkedStack.push(4);

        System.out.println("栈顶元素为： "+linkedStack.peek());
        System.out.println("栈是否为空："+linkedStack.isEmpty());
        System.out.println("栈中元素个数："+linkedStack.size());
        System.out.println(linkedStack.toString());
    }
}
