package jsjf;

public class ArrayStackTest
{
    public static void main(String[] args) {
        ArrayStack arrayStack = new ArrayStack();

        arrayStack.push(2);
        arrayStack.push(6);
        arrayStack.push(4);
        arrayStack.push(8);

        System.out.println("栈顶元素为： "+arrayStack.peek());
        System.out.println("栈是否为空："+arrayStack.isEmpty());
        System.out.println("栈中元素个数："+arrayStack.size());
        System.out.println("栈中元素打印 "+arrayStack.toString());
    }
}
