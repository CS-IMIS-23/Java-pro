package jsjf;

import jsjf.exceptions.EmptyCollectionException;

import java.util.Iterator;

public class LinkedStack<T> implements StackADT<T>
{
    private int count;
    private LinearNode<T> top;

    public LinkedStack() {
        count = 0;
        top = null;
    }

    @Override
    public void push(T element) {
        LinearNode<T> temp = new LinearNode<T>(element);

        temp.setNext(top);
        top = temp;
        count ++;
    }

    @Override
    public T pop() throws EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException("Stack");

        T result = top.getElement();
        top = top.getNext();
        count -- ;

        return result;
    }

    @Override
    public T peek() throws EmptyCollectionException{
        if (isEmpty())
            throw new EmptyCollectionException("Stack");

        T result = top.getElement();

        return result;
    }

    @Override
    public boolean isEmpty() {
        return count==0;
    }

    @Override
    public int size() {
        return count;
    }

    public String toString()
    {
        String result = "";
        LinearNode<T> linearNode = top;
        int i = 0;
        while (i<count)
        {
            result += linearNode.getElement()+" ";
            linearNode = linearNode.getNext();
            i++;
        }
        return result;

    }
}
