import java.util.Random;
import java.text.DecimalFormat;

public class PP39
{
   public static void main(String[] args)
   {
      Random generator = new Random();
      
      int r,h;
      double S,V;

      r = generator.nextInt(20) +1;
      h = generator.nextInt(20) +1;

      System.out.println(r+"\n"+h);

      S = 2*Math.PI*r*h;
      V = Math.PI*Math.pow(r,2)*h;

      DecimalFormat fmt = new DecimalFormat("0.#######");

      System.out.println("The area : " + fmt.format(S));
      System.out.println("The Volume : " + fmt.format(V));
   }
}

