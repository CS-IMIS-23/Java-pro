import java.util.Scanner;

public class PP34
{
   public static void main(String[] args)
   {
      Scanner scan = new Scanner(System.in);

      double x;
      double min , max;

      System.out.print("Enter the \" x \" :");
      x = scan.nextDouble();

      min = Math.floor(x);
      max = Math.ceil(x);

      System.out.println("min : " + min +"\t"+ "max : "+ max);
   }
}
