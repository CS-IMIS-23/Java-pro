import java.util.Scanner;

public class PP35
{
   public static void main(String[] args)
   {
       int x1,y1,x2,y2;
       double  discr,distance;

       Scanner scan = new Scanner(System.in);

       System.out.println("Enter \" x1 \" : " );
       x1 = scan.nextInt();
       
       System.out.println("Enter \" y1 \" : " );
       y1 = scan.nextInt();

       System.out.println("Enter \" x2 \" : " );
       x2 = scan.nextInt();

       System.out.println("Enter \" y2 \" : " );
       y2 = scan.nextInt();

       discr = Math.pow((x1-x2),2)+Math.pow((y1-y2),2);

       distance = Math.sqrt(discr);
      
       System.out.println("The distance : " + distance );
  }
}


