import java.util.Scanner;
import java.text.DecimalFormat;

public class PP36
{
   public static void main(String[] args)
   {
      double r,S,V;
      
      Scanner scan = new Scanner(System.in);
      
      System.out.print("Enter the r : ");
      r = scan.nextDouble();

      V = 4/3 * Math.PI * Math.pow(r,3);
      S = 4 * Math.PI * Math.pow(r,2);

      DecimalFormat fmt = new DecimalFormat("0.####");

      System.out.println("S : "+ fmt.format(S));
      System.out.println("V : "+ fmt.format(V));
      }
   }
