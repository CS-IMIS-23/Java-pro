import java.util.Random;

public class PP38
{
   public static void main(String[] args)
   {
      Random genertor = new Random();
      
      int i,x;
      double sinx,cosx,tanx;

      i = genertor.nextInt(21);

      x = i +20;
      System.out.println("The number of x : " +x);

      sinx = Math.sin(x);
      cosx = Math.cos(x);
      tanx = Math.tan(x);

      System.out.println("sinx: " + sinx + "\n cosx: " +  cosx + 
      "\n tanx:" + tanx);
   }
}
