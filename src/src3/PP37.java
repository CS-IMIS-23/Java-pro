import java.util.Scanner;
import java.text.DecimalFormat;

public class PP37
{
   public static void main(String[] args)
   {
      double a,b,c,s,S;

      Scanner scan = new Scanner(System.in);

      System.out.print("Enter a : ");
      a = scan.nextDouble();

      System.out.print("Enter b : ");
      b = scan.nextDouble();  

      System.out.print("Enter c : ");
      c = scan.nextDouble();

      s = (a+b+c)/2;

      S = Math.sqrt(s*(s-a)*(s-b)*(s-c));

      DecimalFormat fmt = new DecimalFormat("0.###");

      System.out.println("The area is :"+ fmt.format(S));
   }
}
