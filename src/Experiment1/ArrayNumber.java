package Experiment1;

import java.io.*;
import java.util.Scanner;

public class ArrayNumber {
    public static void main(String[] args)throws IOException {
        int[] array = new int[50];
        String message = "";
        int nHouZeYang = 0;
        LinearNode<Integer> linearNode = null;

        Scanner scanner = new Scanner(System.in);

        System.out.println("输入你的学号和当前时间： ");
        message = scanner.nextLine();

        for (int i = 0; i < message.length(); i = i + 2) {
            int number = Integer.parseInt(message.substring(i, i + 2));
            array[nHouZeYang] = number;
            nHouZeYang++;
        }

        String result = "";
        for (int i = 0; i < nHouZeYang; i++) {
            result += array[i] + " ";
        }
        System.out.println("所有数组元素为：" + result);
        System.out.println("元素个数为：" + nHouZeYang);

        //从文件中读取数字1和数字2，并将它们储存在int型数组中
        File file = new File("D:\\JAVA程序设计\\Java-pro\\text.txt");
        Reader reader = null;
        try {
            System.out.println("以字符为单位读取文件内容，一次读一个字节：");
            reader = new InputStreamReader(new FileInputStream(file));
            int tempchar;
            int[] nums = new int[2];
            int x = 0;
            while ((tempchar = reader.read()) != -1)
            {
                if (((char) tempchar) != '\r'&&((char) tempchar) != '\n')
                {
                    {
                        nums[x]=tempchar-(int)('0');
                        System.out.print((nums[x])+" ");
                        x++;
                    }
                }
            }
            reader.close();

            //将数字1插入到链表第5位，并打印所有数字，和元素的总数
            int index = 4;
            for (int y = nHouZeYang;y>=4;y--)
            {
                array[y+1] = array[y];
            }
            array[index] = nums[0];
            nHouZeYang++;
            result = "";
            for (int i = 0; i < nHouZeYang; i++) {
                result += array[i] + " ";
            }
            System.out.println("插入数字1后所有数组元素为：" + result);
            System.out.println("插入数字1后元素个数为：" + nHouZeYang);

            //从文件中读入数字2， 插入到数组第 0 位，并打印所有数字，和元素的总数
            index = 0;
            for (int y = nHouZeYang;y>=0;y--)
            {
                array[y+1] = array[y];
            }
            array[index] = nums[1];
            nHouZeYang++;
            result = "";
            for (int i = 0; i < nHouZeYang; i++) {
                result += array[i] + " ";
            }
            System.out.println("插入数字2后所有数组元素为：" + result);
            System.out.println("插入数字2后元素个数为：" + nHouZeYang);

            //从数组中删除刚才的数字1.  并打印所有数字和元素的总数
            int num1 = 0;
            while (array[num1]!=1)
                num1++;//确定数字1所在位置

            for (int i=num1;i<=nHouZeYang;i++)
            {
                array[i] = array[i+1];
            }
            nHouZeYang--;
            result = "";
            for (int i = 0; i < nHouZeYang; i++) {
                result += array[i] + " ";
            }
            System.out.println("删除数字1后所有数组元素为：" + result);
            System.out.println("删除数字1后后元素个数为：" + nHouZeYang);


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        //使用插入排序法对数组中元素进行排序

        for (int index = 1; index <nHouZeYang; index++)
        {
            int key = array[index];
            int position = index;

            //  Shift larger values to the right
            while (position > 0 && key<array[position-1])
            {
                array[position] = array[position-1];
                position--;
            }

            array[position] = key;

            result = "";
            for (int i = 0; i < nHouZeYang; i++) {
                result += array[i] + " ";
            }
            System.out.println("排序中所有数组元素为：" + result);
            System.out.println("排序中元素个数为：" + nHouZeYang);
        }




    }
}
