package Experiment1;

public class LinearNode<T> {
    public int number;
    public LinearNode next;

    public LinearNode()
    {
        next = null;
    }
    public LinearNode(int number) {
        this.number = number;
        next = null;
    }

}
