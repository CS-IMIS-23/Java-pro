package Experiment1;

import java.io.*;
import java.util.Scanner;

public class NumList
{
    public static void main(String[] args) throws IOException {
        String message ="";
        int nHouZeYang = 0;
        LinearNode<Integer> linearNode = null;

        Scanner scanner = new Scanner(System.in);

        System.out.println("输入你的学号和当前时间： ");
        message = scanner.nextLine();

        for(int i=0;i<message.length();i=i+2)
        {
            int number = Integer.parseInt(message.substring(i,i+2));
            LinearNode<Integer> tLinearNode = new LinearNode<Integer>(number);
            if(linearNode==null)
                linearNode=tLinearNode;
            else
            {
                LinearNode<Integer> current = linearNode;
                while (current.next != null)
                    current = current.next;
                current.next = tLinearNode;

            }
            nHouZeYang++;
        }

        String result = "";
        LinearNode<Integer> integerLinearNode = linearNode;
        int i = 0;
        while (i<nHouZeYang)
        {
            result += integerLinearNode.number+" ";
            integerLinearNode = integerLinearNode.next;
            i++;
        }

        System.out.println("所有链表元素为："+result);
        System.out.println("元素个数为："+nHouZeYang);

        //从文件中读取数字1和数字2，并将它们储存在int型数组中
        File file = new File("D:\\JAVA程序设计\\Java-pro\\text.txt");
        Reader reader = null;
        try {
            System.out.println("以字符为单位读取文件内容，一次读一个字节：");
            reader = new InputStreamReader(new FileInputStream(file));
            int tempchar;
            int[] array = new int[2];
            int x = 0;
            while ((tempchar = reader.read()) != -1)
            {
                if (((char) tempchar) != '\r'&&((char) tempchar) != '\n')
                {
                    {
                        array[x]=tempchar-(int)('0');
                        System.out.print((array[x])+" ");
                        x++;
                    }
                }
            }
            reader.close();

            //将数字1插入到链表第5位，并打印所有数字，和元素的总数
            int index = 4;

            System.out.println();
            LinearNode<Integer> current = linearNode;
            LinearNode linearNode1 = new LinearNode(array[0]);
            for (int y= 0;y<index-1;y++)
            {
                current = current.next;
            }
            linearNode1.next = current.next;
            current.next = linearNode1;
            nHouZeYang++;
            result = "";
            i = 0;
            integerLinearNode = linearNode;
            while (i<nHouZeYang)
            {
                result += integerLinearNode.number+" ";
                integerLinearNode = integerLinearNode.next;
                i++;
            }
            System.out.println("插入数字1后所有链表元素为："+result);
            System.out.println("插入数字1后元素个数为："+nHouZeYang);

            // 将将数字2插入到链表第0位，并打印所有数字，和元素的总数
            LinearNode linearNode2 = new LinearNode(array[1]);
            System.out.println(array[1]);
            current = linearNode;
            linearNode = linearNode2;
            linearNode.next = current;
            nHouZeYang++;
            result = "";
            i = 0;
            integerLinearNode = linearNode;
            while (i<nHouZeYang)
            {
                result += integerLinearNode.number+" ";
                integerLinearNode = integerLinearNode.next;
                i++;
            }
            System.out.println("插入数字2后所有链表元素为："+result);
            System.out.println("插入数字2后元素个数为："+nHouZeYang);


            //从链表中删除刚才的数字1.  并打印所有数字和元素的总数

            current = linearNode ;
            while (current.next!=linearNode1)
            {
                current = current.next;
            }
            current.next = current.next.next;

            nHouZeYang--;
            result = "";
            i = 0;
            integerLinearNode = linearNode;
            while (i<nHouZeYang)
            {
                result += integerLinearNode.number+" ";
                integerLinearNode = integerLinearNode.next;
                i++;
            }
            System.out.println("删除数字1后所有链表元素为："+result);
            System.out.println("删除数字1后元素个数为："+nHouZeYang);

        } catch (Exception e) {
            e.printStackTrace();
        }
        // 使用选择排序法对链表进行排序
        LinearNode<Integer> current = linearNode;

       while (current.next!=null)
       {
           LinearNode temp = current.next;
           int minNumber = current.number;
           while (temp!=null)
           {
               if(minNumber>temp.number)
               {
                   minNumber = temp.number;
               }
               temp = temp.next;
           }

           LinearNode current0 = current;

           while (current0.number!=minNumber)
           {
               current0 = current0.next;
           }
           current0.number = current.number;
           current.number = minNumber;
           result = "";
           i = 0;
           integerLinearNode = linearNode;
           while (i<nHouZeYang)
           {
               result += integerLinearNode.number+" ";
               integerLinearNode = integerLinearNode.next;
               i++;
           }
           System.out.println("排序中所有链表元素为："+result+" 元素个数为："+nHouZeYang);
           System.out.println();

           current = current.next;
       }
    }
}
