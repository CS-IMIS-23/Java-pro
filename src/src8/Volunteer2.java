package src8;

public class Volunteer2 implements Payable
{
    protected String name;
    protected String address;
    protected String phone;
    public Volunteer2(String eName, String eAddress, String ePhone)
    {
        name = eName;
        address = eAddress;
        phone = ePhone;
    }

    @Override
    public double pay() {
        return 0.0;
    }
}
