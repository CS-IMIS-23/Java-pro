package src8;

public class Employee2 implements Payable
{
    protected String name;
    protected String address;
    protected String phone;
    protected String socialSecurityNumber;
    protected double payRate;

    //-----------------------------------------------------------------
    //  Constructor: Sets up this employee with the specified
    //  information.
    //-----------------------------------------------------------------
    public Employee2(String eName, String eAddress, String ePhone,
                    String socSecNumber, double rate)
    {
        name = eName;
        address = eAddress;
        phone = ePhone;

        socialSecurityNumber = socSecNumber;
        payRate = rate;
    }

    //-----------------------------------------------------------------
    //  Returns information about an employee as a string.
    //-----------------------------------------------------------------
    public String toString()
    {
        String result = super.toString();

        result += "\nSocial Security Number: " + socialSecurityNumber;

        return result;
    }

    //-----------------------------------------------------------------
    //  Returns the pay rate for this employee.
    //-----------------------------------------------------------------

    @Override
    public double pay() {
        return payRate;
    }
}
