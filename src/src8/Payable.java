package src8;

public interface Payable
{
    public String toString();
    public  double pay();
}
