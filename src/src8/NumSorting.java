package src8;

public class NumSorting
{
    public static void main(String[] args) {
        int[] number1 = {34,35,68,23,12,57,36,98,4,8,20,13,45};
        Sorting2.selectionSort(number1);
        for(int num:number1)
            System.out.print(num+" ");
        System.out.println();
        int[] number2 = {9,3,657,767,42,12,646,34,98,90,99,1000,23,431,24};
        Sorting2.insertionSort(number2);
        for(int num:number2)
            System.out.print(num+" ");


    }
}
