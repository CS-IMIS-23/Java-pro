package src6;

public class Transactions
{
      private Account[] acct;
      private double interest ;
      private int num;
      public Transactions()
      {
          acct = new Account[30];
          interest = 0.030;
          num=0;
      }
      public void addUser(String owner,long account,double initial)
      { Account user = new Account(owner, account, initial);
          acct[num] = user;
          num++;
      }
      public void addallInterest()
      {
          for (int number = 0;number<num;num++)
          {
              acct[num].addInterest();
          }
      }
      public double acctdeposit(int number,double amount)
      {
          double acctbalance = acct[number].deposit(amount);
          return  acctbalance;
      }
      public double acctwithdraw(int number,double amount,double fee)
      {
          double acctbanlance = acct[number].withdraw(amount,fee);
          return acctbanlance;
      }


      public String toString()
      {
          String result="";
          
          for (int index=0;index<acct.length;index++) 
              result +=acct[index].toString()+"\n";
          return result;
      }
}
