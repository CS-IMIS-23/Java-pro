public class HuffmanTreeNode implements Comparable<HuffmanTreeNode>
{
    private int weight;
    private HuffmanTreeNode parent,left,right;
    private char element;

    public HuffmanTreeNode(int weight, char element ,HuffmanTreeNode parent, HuffmanTreeNode left, HuffmanTreeNode right) {
        this.weight = weight;
        this.element = element;
        this.parent = parent;
        this.left = left;
        this.right = right;
    }

    @Override
    public int compareTo(HuffmanTreeNode huffmanTreeNode) {
        return this.weight - huffmanTreeNode.getWeight();
    }

    public int getWeight() {
        return weight;
    }


    public HuffmanTreeNode getParent() {
        return parent;
    }

    public void setParent(HuffmanTreeNode parent) {
        this.parent = parent;
    }

    public HuffmanTreeNode getLeft() {
        return left;
    }

    public void setLeft(HuffmanTreeNode left) {
        this.left = left;
    }

    public HuffmanTreeNode getRight() {
        return right;
    }

    public void setRight(HuffmanTreeNode right) {
        this.right = right;
    }

    public char getElement() {
        return element;
    }

    public void setElement(char element) {
        this.element = element;
    }
}
