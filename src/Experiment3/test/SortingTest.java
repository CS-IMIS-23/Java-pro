package Experiment3.test;

import Experiment3.cn.edu.besti.cs1723.H2302.Sorting;
import junit.framework.TestCase;
import org.junit.Test;


public class SortingTest extends TestCase
{

    @Test
    public void testNormal() throws Exception
    {
        Integer[] integers1 = {2,7,12,13,14,23,29,32,46,56};
        Integer[] integers2 = {7,32,12,46,14,56,29,13,23,2};
        Sorting.selectionSort(integers2);
        for (int i=0;i<integers2.length;i++)
             assertEquals(integers1[i],integers2[i]);

        Integer[] integers3 = {2,7,12,13,14,23,29,32,46,53};
        Integer[] integers4 = {7,32,12,46,14,53,29,13,23,2};
        Sorting.selectionSort(integers4);
        for (int i=0;i<integers4.length;i++)
            assertEquals(integers3[i],integers4[i]);

        Integer[] integers5 = {2,7,12,13,17,23,29,32,46,53};
        Integer[] integers6 = {7,32,12,46,17,53,29,13,23,2};
        Sorting.selectionSort(integers6);
        for (int i=0;i<integers6.length;i++)
            assertEquals(integers5[i],integers6[i]);

    }
    @Test
    public void testAbnormal() throws Exception
    {
        Integer[] integers1 = {2,7,12,13,14,23,29,32,46,56};
        Integer[] integers2 = {7,32,12,46,14,56,29,14,23,2};
        Sorting.selectionSort(integers2);
        boolean result1 = true;
        for (int i =0 ;i<integers2.length;i++)
        {
            if (integers1[i]!=integers2[i])
            {
                result1 = false;
                break;
            }
        }
        assertEquals(false,result1);

        Integer[] integers3 = {2,7,12,13,14,23,29,32,46,56};
        Integer[] integers4 = {7,32,19,46,14,56,29,13,23,2};
        Sorting.selectionSort(integers4);
        boolean result2 = true;
        for (int i =0 ;i<integers4.length;i++)
        {
            if (integers3[i]!=integers4[i])
            {
                result2 = false;
                break;
            }
        }
        assertEquals(false,result2);

        Integer[] integers5 = {2,7,12,13,14,23,24,32,46,56};
        Integer[] integers6 = {7,32,12,46,14,56,29,13,23,2};
        Sorting.selectionSort(integers5);
        boolean result3 = true;
        for (int i =0 ;i<integers5.length;i++)
        {
            if (integers5[i]!=integers6[i])
            {
                result3 = false;
                break;
            }
        }
        assertEquals(false,result3);

        Integer[] integers7 = {2,6,12,13,14,23,24,32,46,56};
        Integer[] integers8 = {7,32,12,46,14,56,29,13,23,2};
        Sorting.selectionSort(integers8);
        boolean result4 = true;
        for (int i =0 ;i<integers8.length;i++)
        {
            if (integers7[i]!=integers8[i])
            {
                result4 = false;
                break;
            }
        }
        assertEquals(false,result4);
    }
    @Test
    public void testBoundary () throws Exception{
        Integer[] integers1 = {2,7,12,13,14,23,29,32,46,8961};
        Integer[] integers2 = {7,32,12,46,14,8961,29,14,23,2};
        Sorting.selectionSort(integers2);
        boolean result1 = true;
        for (int i =0 ;i<integers2.length;i++)
        {
            if (integers1[i]!=integers2[i])
            {
                result1 = false;
                break;
            }
        }
        assertEquals(false,result1);

        Integer[] integers3 = {-139409,7,12,13,14,23,29,32,46,56};
        Integer[] integers4 = {7,32,19,46,14,56,29,13,23,-139409};
        Sorting.selectionSort(integers4);
        boolean result2 = true;
        for (int i =0 ;i<integers4.length;i++)
        {
            if (integers3[i]!=integers4[i])
            {
                result2 = false;
                break;
            }
        }
        assertEquals(false,result2);
    }
    @Test
    public void testReversed() throws Exception
    {

        Integer[] integers1 = {56,46,32,29,23,14,13,12,7,2};
        Integer[] integers2 = {7,32,12,46,14,56,29,13,23,2};
        Sorting.selectionSort(integers2);
        for (int i=0;i<integers2.length;i++)
            assertEquals(integers1[integers1.length-1-i],integers2[i]);

        Integer[] integers3 = {56,45,32,29,23,14,13,12,7,2};
        Integer[] integers4 = {7,32,12,45,14,56,29,13,23,2};
        Sorting.selectionSort(integers4);
        for (int i=0;i<integers4.length;i++)
            assertEquals(integers3[integers3.length-1-i],integers4[i]);

        Integer[] integers5 = {56,45,32,27,23,14,13,12,7,2};
        Integer[] integers6 = {7,32,12,45,14,56,27,13,23,2};
        Sorting.selectionSort(integers6);
        for (int i=0;i<integers5.length;i++)
            assertEquals(integers5[integers5.length-1-i],integers6[i]);

    }

}
