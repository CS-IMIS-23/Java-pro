package Experiment3.test;

import Experiment3.cn.edu.besti.cs1723.H2302.Searching;
import junit.framework.TestCase;
import org.junit.Test;


public class SearchingTest extends TestCase
{
    Integer[] integers = {7,32,12,46,14,56,29,13,23,2};

    @Test
    public void testNormal() throws Exception
    {
        assertEquals(true, Searching.linearSearch(integers,0,integers.length-1,14));
        assertEquals(true,Searching.linearSearch(integers,0,integers.length-1,46));
        assertEquals(true,Searching.linearSearch(integers,0,integers.length-1,29));
        assertEquals(true,Searching.linearSearch(integers,0,integers.length-1,13));
    }
    @Test
    public void testAbnormal() throws Exception{
        assertEquals(false, Searching.linearSearch(integers,0,integers.length-1,34));
        assertEquals(false, Searching.linearSearch(integers,0,integers.length-1,100));
        assertEquals(false, Searching.linearSearch(integers,0,integers.length-1,300));
        assertEquals(false, Searching.linearSearch(integers,0,integers.length-1,500));
    }

    @Test
    public void testBoundary () throws Exception{
        assertEquals(true, Searching.linearSearch(integers,0,integers.length-1,7));
        assertEquals(true, Searching.linearSearch(integers,0,integers.length-1,2));
    }
}
