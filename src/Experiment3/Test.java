package Experiment3;

import Experiment3.cn.edu.besti.cs1723.H2302.Searching;
import Experiment3.cn.edu.besti.cs1723.H2302.Sorting;

public class Test
{
    public static void main(String[] args) {
        Integer[] integers = {7,32,12,46,14,56,29,13,23,2};

        System.out.println("线性查找测试：");
        System.out.println("是否含有元素14： "+Searching.linearSearch(integers,0,integers.length-1,14));
        System.out.println("是否含有元素2："+Searching.linearSearch(integers,0,integers.length-1,2));
        System.out.println("是否含有元素90："+Searching.linearSearch(integers,0,integers.length-1,90));
        System.out.println();

        System.out.println("树表查找测试：");
        System.out.println("是否含有元素14： "+Searching.binaryTreeSearch(integers,0,integers.length-1,14));
        System.out.println("是否含有元素2："+Searching.binaryTreeSearch(integers,0,integers.length-1,2));
        System.out.println("是否含有元素90："+Searching.binaryTreeSearch(integers,0,integers.length-1,90));
        System.out.println();

        System.out.println("分块查找测试：");
        System.out.println("是否含有元素14： "+Searching.partitionSearch(integers,0,integers.length-1,14));
        System.out.println("是否含有元素2："+Searching.partitionSearch(integers,0,integers.length-1,2));
        System.out.println("是否含有元素90："+Searching.partitionSearch(integers,0,integers.length-1,90));
        System.out.println();

        Integer[] integers2 = {7,32,12,46,14,56,29,13,23,2};
        Sorting.selectionSort(integers2);
        System.out.println("二分查找、插值查找、斐波那契查找均基于已排序序列，先对序列进行排序。");

        System.out.println("二分查找测试：");
        System.out.println("是否含有元素14： "+Searching.binarySearch(integers2,0,integers.length-1,14));
        System.out.println("是否含有元素2："+Searching.binarySearch(integers2,0,integers.length-1,2));
        System.out.println("是否含有元素90："+Searching.binarySearch(integers2,0,integers.length-1,90));
        System.out.println();

        System.out.println("插值查找测试：");
        System.out.println("是否含有元素14： "+Searching.insertionSearch(integers2,0,integers.length-1,14));
        System.out.println("是否含有元素2："+Searching.insertionSearch(integers2,0,integers.length-1,2));
        System.out.println("是否含有元素90："+Searching.insertionSearch(integers2,0,integers.length-1,90));
        System.out.println();

        System.out.println("斐波那契查找测试：");
        System.out.println("是否含有元素14： "+Searching.FibonacciSearch(integers2,0,integers.length-1,14));
        System.out.println("是否含有元素2："+Searching.FibonacciSearch(integers2,0,integers.length-1,2));
        System.out.println("是否含有元素90："+Searching.FibonacciSearch(integers2,0,integers.length-1,90));
        System.out.println();



        int[] ints = {7,32,12,46,14,56,29,13,23,2};
        System.out.println("哈希查找测试：");
        System.out.println("是否含有元素14： "+Searching.hashSearch(ints,0,integers.length-1,14));
        System.out.println("是否含有元素2："+Searching.hashSearch(ints,0,integers.length-1,2));
        System.out.println("是否含有元素90："+Searching.hashSearch(ints,0,integers.length-1,90));
    }
}
