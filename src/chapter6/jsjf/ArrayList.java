package chapter6.jsjf;

import chapter6.jsjf.exceptions.ElementNotFoundException;
import chapter6.jsjf.exceptions.EmptyCollectionException;

import java.util.Iterator;

public abstract class ArrayList<T> implements ListADT<T>,Iterable<T>
{
    private final static int DEFAULT_CAPACITY = 100;
    private final static int NOT_FOUND = -1;

    protected int rear;
    protected T[] list;
    protected int modCount;

    /**
     * Creates an empty list using the default capacity.
     */
    public ArrayList()
    {
        this(DEFAULT_CAPACITY);
    }

    /**
     * Creates an empty list using the specified capacity.
     *
     * @param initialCapacity the integer value of the size of the array list
     */
    public ArrayList(int initialCapacity)
    {
        rear = 0;
        list = (T[])(new Object[initialCapacity]);
        modCount = 0;
    }

    protected void expandCapacity()
    {
        // To be completed as a Programming Project
    }

    public T remove(T element)
    {
        T result;
        int index = find(element);

        if (index == NOT_FOUND)
            throw new ElementNotFoundException("ArrayList");

        result = list[index];
        rear--;

        // shift the appropriate elements
        for (int scan=index; scan < rear; scan++)
            list[scan] = list[scan+1];

        list[rear] = null;
        modCount++;

        return result;
    }
    public boolean contains(T target)
    {
        return (find(target) != NOT_FOUND);
    }

    /**
     * Returns the array index of the specified element, or the
     * constant NOT_FOUND if it is not found.
     *
     * @param target the target element
     * @return the index of the target element, or the
     *         NOT_FOUND constant
     */
    private int find(T target)
    {
        int scan = 0;
        int result = NOT_FOUND;

        if (!isEmpty())
            while (result == NOT_FOUND && scan < rear)
                if (target.equals(list[scan]))
                    result = scan;
                else
                    scan++;

        return result;
    }

    public T removeFirst() {
        T result;
        int index = 0;

        if (rear == 0)
            throw new EmptyCollectionException("ArrayList");
        else {
            result = list[index];
            rear--;

            // shift the appropriate elements
            for (int scan=index; scan < rear; scan++)
                list[scan] = list[scan+1];

            list[rear] = null;
            modCount++;

            return result;
        }
    }


    public T removeLast() {
        T result;
        int index = rear;

        if (rear == 0)
            throw new EmptyCollectionException("ArrayList");
        else {
            result = list[index];
            rear--;

            list[rear] = null;
            modCount++;

            return result;
        }
    }


    public T first() {
            T result;
            int index = 0;

            if (rear == 0)
                throw new EmptyCollectionException("ArrayList");
            else {
                result = list[index];
                return result;
            }
    }


    public T last() {
        T result;
        int index = rear;

        if (rear == 0)
            throw new EmptyCollectionException("ArrayList");
        else {
            result = list[index];
            return result;
        }
    }


    public boolean isEmpty() {
        return rear==0;
    }


    public int size() {
        return rear;
    }

}
