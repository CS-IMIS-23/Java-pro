package chapter6.jsjf;

public interface OrderedListADT<T> extends ListADT
{
    public void add(T element);
}
