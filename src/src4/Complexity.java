package src4;

public interface Complexity
{
    public void setComplexity(int complexity);
    public int getComplexity();
}

