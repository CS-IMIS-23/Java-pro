//**************************************************************
//   PP4.1                CounterTest
//**************************************************************

public class PP41
{
   public static void main(String[] args)
   {
      Counter cou1,cou2 ;

      cou1 = new Counter();
      cou2 = new Counter();

      System.out.println("The initial number1 : " +cou1.getCount());
      System.out.println("The initial number1 : " +cou2.getCount());
      
      cou1.click();
      cou2.click();

      System.out.println("Number1 : " +cou1+ "\n"+ "Number2 : "+cou2);

      //---------------------------------------
      // Reset number1 .
      //---------------------------------------
       
      cou1.reset();

      System.out.print("Number1 : " +cou1+ "\n"+ "Number2 : "+cou2);
    }
}

