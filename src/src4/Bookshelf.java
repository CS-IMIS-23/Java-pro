public class Bookshelf 
{
  public static void main(String[] args)
  {
     Book bookshelf  = new Book();

     //------------------------------------------
     // Print the book's basic information.
     //------------------------------------------

     System.out.println(bookshelf.getBookName());
     
     System.out.println(bookshelf.getAuthor());
     
     System.out.println(bookshelf.getPress());
     
     System.out.println(bookshelf.getCopyrightDate());
    
    //---------------------------------------------
    //  Change the information and print it .
    //---------------------------------------------

     bookshelf.setBookName("Java程序设计教程");
     System.out.println("Name : "+bookshelf.getBookName());

     
     bookshelf.setAuthor("JohnLewis");
     System.out.println("Author :" +bookshelf.getAuthor());
     
     
     bookshelf.setPress("2017.1");
     System.out.println("Press : "+bookshelf.getPress());
     
     
     bookshelf.setCopyrightDate("2015.7");
     System.out.println("Copyrightdate : "+bookshelf.getCopyrightDate());

 }  
}
    
