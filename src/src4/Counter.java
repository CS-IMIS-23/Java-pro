//*************************************************************
//    Counter.java      Class for PP4.1
//*************************************************************

public class Counter
{
   private int number ;// the number of people;

   //---------------------------------------------
   //    Set the initial number .
   //---------------------------------------------

   public Counter()
   {
       number = 0;
   }

   public int click()
   {
      number = number+1 ;
      return number ;
   }

   public int getCount()
   {
      return number ;
   }
   
   public int reset()
   {
     number = 0 ;
     return number ;
   }

   public String toString()
   {
      String result = Integer.toString(number);

      return result;
   }
}




   

