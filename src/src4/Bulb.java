//************************************************
//   Bulb.java       Class for lights(PP4.2)
//************************************************

public class Bulb
{
   private boolean light;

   public Bulb()
   {
     light = false ;
   }
   
   //---------------------------
   // Turn on the light.
   //---------------------------

   public boolean turnon()
   {
     light = true ;
     
     return light ;
   }

   //---------------------------
   // Turn off the light.
   //---------------------------

   public boolean turnoff()
   {
     light = false ;

     return light ;
   }

   //-----------------------------------------------
   // Returns a string representation of this light.
   //-----------------------------------------------

   public Boolean toBoolean()
   {
     return light ;
   }
}

