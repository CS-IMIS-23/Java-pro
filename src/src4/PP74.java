package src4;
import java.math.BigDecimal;
import java.lang.Comparable;
public class PP74
{
    private int numerator,denominator;
    double number1,number2;
    public PP74(int numer,int denom)
    {
        if (denom ==0)
            denom = 1;

        if (denom <0)
        {
            numer =numer* -1;
            numer =denom* -1;
        }

        numerator = numer;
        denominator = denom;
        reduce();
    }

    public int getNumerator()
    {
        return numerator;
    }

    public int getDenominator()
    {
        return denominator;
    }

    public PP74 reciprocal()
    {
        return new PP74(denominator,numerator);
    }

    public PP74 add(PP74 op2)
    {
        int commonDenominator = denominator * op2.getDenominator();
        int numerator1 = numerator * op2.getDenominator();
        int numerator2 = op2.getNumerator() * denominator;
        int sum = numerator1 + numerator2;

        return new PP74(sum,commonDenominator);
    }

    public PP74 subtract(PP74 op2)
    {
        int commonDenominator = denominator * op2.getDenominator();
        int numerator1 = numerator * op2.getDenominator();
        int numerator2 = op2.getNumerator() * denominator;
        int difference = numerator1 -numerator2;

        return new PP74(difference,commonDenominator);
    }

    public PP74 multiply(PP74 op2)
    {
        int numer = numerator *op2.getNumerator();
        int denom = denominator * op2.getDenominator();

        return new PP74(numer,denom);
    }

    public PP74 divide(PP74 op2)
    {
        return multiply(op2.reciprocal());
    }

    public boolean isLike(PP74 op2)
    {
        return (numerator ==op2.getNumerator() &&
                denominator == op2.getDenominator() );
    }


    public String toString()
    {
        String result;
        if(numerator ==0)
            result = "0";
        else
        if (denominator == 1)
            result = numerator + "";
        else
            result= numerator + "/"+denominator;
        return result;
    }

    private void reduce()
    {
        if (numerator !=0)
        {
            int common = gcd(Math.abs(numerator),denominator);

            numerator = numerator/common;
            denominator = denominator/common;
        }
    }

    private int gcd(int num1,int num2)
    {
        while (num1 !=num2)
            if (num1>num2)
                num1=num1 - num2;
            else
                num2 = num2 - num1;

        return num1;
    }
        public int compareTo(PP74 op2) {
            double a = numerator / denominator;
            double b = op2.getNumerator() / op2.getDenominator();
            int number;
            if (Math.abs(a - b) <= 0.0001)
                number = 0;
            else if (a - b > 0.0001)
                number = 1;
            else
                number = -1;
            return number;
        }
}




