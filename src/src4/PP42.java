//******************************************************
//  PP4.2.java        Lights for test class Bulb.
//******************************************************

public class PP42
{
   public static void main(String[] args)
   {
   
     Bulb light1 = new Bulb();
     Bulb light2 = new Bulb();

     System.out.println("The state of light1 : " +light1.turnon());
     System.out.println("The state of light2 : " +light2.turnon());

     System.out.println("light1 : "+light1.turnon()+"\n"+"light2 : "+ light2.turnoff());

     System.out.println("turn off light1 :"+light1.turnoff());
   }
}

