//*************************************************************************
//   PairOfDice                      Class for RollingDice2.
//*************************************************************************

public class PairOfDice
{
    private final int MAX=6;

    private int facevalue1,facevalue2;

    public PairOfDice()
    {
      facevalue1 = 1 ;
      facevalue2 = 1 ;
    }
    
    public int roll()
    {
      facevalue1 = (int)(Math.random()*MAX)+1;
      return facevalue1 ;
    }

    public void setFaceValue1(int value1)
    {
        facevalue1 = value1 ;
    }

    public void setFaceValue2(int value2)
    {
       facevalue2 = value2 ;
    }
    

    public int getFaceValue1()
    {
       return facevalue1;
    }
    
    public int getFaceValue2()
    {
       return facevalue2;
    }

    public int getSum()
    {
       int sum ;
       sum = facevalue1 + facevalue2;
       return sum ;

    }
}    
    
