//****************************************************************
//      Car.java                    Class for CarTest.
//****************************************************************

public class Car
{
   private String tradename ;
   
   private String model;
   
   private int yotf ;// Year of the factory .
   
   private boolean yon ; // yes or no.

   public  Car()
   {
     tradename = "VW";
     model = "Passat";
     yotf = 1950 ;
     yon = false;
   }

   public String getTradeName()
   {
     return tradename ;
   }

   public String getModel()
   {
     return model ;
   }

   public int getYotf()
   {
     return yotf ;
   }
   
   public boolean isAntique()
   {
     yon = 2018-yotf > 45 ;
     
     return yon ;
   }
    
   public String toString()
   {
      String result = Integer.toString(yotf);

      return tradename + model + result ;
   }
   public Boolean toBoolean()
   {
      return yon ;
   }
}
