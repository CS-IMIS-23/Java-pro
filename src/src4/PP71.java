package src4;
public class PP71
{
    //---------------------------------------------------------
    //Creats some bank accounts and requests various services.
    //---------------------------------------------------------
    public static void main(String[] args)
    {
        Account71 acct1 = new Account71("Ted Murphy",72354,102.56);
        Account71 acct2 = new Account71("Jane Smith", 69713,40.00);
        Account71 acct3 = new Account71("Edward Demsey ",93757,759.32);
        Account71 acct4 = new Account71( "Skura",16281);

        acct1.deposit(25.85);
        acct4.deposit(1000);

        double smithBalance = acct2.deposit(500.00);
        System.out.println("Smith balance after deposit : "+ smithBalance);

        System.out.println("Smith balance after withdrawal :"+acct2.withdraw(430.75,1.50));

        acct1.addInterest();
        acct2.addInterest();
        acct3.addInterest();
        acct4.addInterest();

        System.out.println();
        System.out.println(acct1);
        System.out.println(acct2);
        System.out.println(acct3);
        System.out.println(acct4);
    }
}

