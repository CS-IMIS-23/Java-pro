//**************************************************************
//   PP4.9               Test Pair of dice .
//**************************************************************

public class PP49
{
   public static void main(String[] args)
   {
      PairOfDice die = new PairOfDice();

      System.out.println("Die1 : "+die.roll());

      die.roll();
      System.out.println("Die1 : "+die.getFaceValue1());

      die.setFaceValue2(5);
      System.out.println("Die2 :" +die.getFaceValue2());

      System.out.println("sum : "+die.getSum());

   }
}
