public class Book
{
   private String bookname;
   
   private String author;
   
   private String press;
   
   private String copyrightdate;

   public Book()
   {
     bookname = "hh";
     author = "zz" ;
     press ="yy" ;
     copyrightdate = "hhzzyy";
   }

   //-------------------------------------------
   //  The book basic information.
   //-------------------------------------------
   
   public String getBookName()
   {
     return bookname ;
   }
   public String getAuthor()
   {
     return author ;
   }
   public String getPress()
   {
     return press ;
   }
   public String getCopyrightDate()
   {
     return copyrightdate;
   }
   public void setBookName(String message)
   {
     bookname = message ;
   }
   public void setAuthor(String message)
   {
     author = message ;
   }
   public void setPress(String message)
   {
     press = message ;
   }
   public void setCopyrightDate(String message)
   {
     copyrightdate = message ;
   }

   public String toString()
   {
     return bookname + author + press + copyrightdate;
   }
}
