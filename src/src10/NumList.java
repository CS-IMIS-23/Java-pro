package src10;

import javax.print.attribute.standard.NumberUp;

public class NumList {
    private NumNode list;

    public NumList() {
        list = null;
    }

    public void add(int mag) {
        NumNode node = new NumNode(mag);
        NumNode current;
        if (list == null)
            list = node;
        else {
            current = list;
            while (current.next != null)
                current = current.next;
            current.next = node;
        }
    }
    public void selectionSort()
    {
        NumNode min;
        int temp;
        NumNode numNode = list;
        while (numNode!=null)
        {
            min = numNode;
            NumNode current = min.next;
            while (current!=null)
            {
                 if(current.number>min.number)
                     min = current ;
                 current = current.next;
            }
            temp= min.number;
            min.number = numNode.number;
            numNode.number = temp;

            numNode  = numNode.next;
        }
    }

    public String toString() {
        String result = "";

        NumNode current = list;
        while (current != null) {
            result += current.number + " ";
            current = current.next;
        }
        return result;
    }

    private class NumNode {
        public int number;
        public NumNode next;

        public NumNode(int number) {
            this.number = number;
            next = null;
        }
    }
}
