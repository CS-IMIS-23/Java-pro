package src10;
public class MagazineRack
{
   public static void main(String[] args)
   {    
      MagazineList rack = new MagazineList();
      Magazine magazine = new Magazine("Magazine");
      Magazine newmagazine = new Magazine("newmagazine");

      rack.add(new Magazine("Time"));
      rack.add(new Magazine("Woodworking Today"));
      rack.add(new Magazine("Communications of the ACM"));
      rack.add(new Magazine("Reader"));
      rack.add(new Magazine("House and Garden"));
      rack.add(new Magazine("GQ"));
      System.out.println("初始: "+rack);

      rack.insert(4,magazine);
      rack.insert(0,newmagazine);
      System.out.println("插入两个后: "+rack);

      rack.delete(newmagazine);
      rack.delete(magazine);
      System.out.println("删除插入的两个后:"+rack);

      System.out.println("排序后： ");
      rack.Sort();
   }
}
