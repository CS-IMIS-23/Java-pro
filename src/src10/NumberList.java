package src10;

public class NumberList
{
    public static void main(String[] args) {
        NumList numList = new NumList();

        numList.add(2);
        numList.add(9);
        numList.add(89);
        numList.add(-1);
        numList.add(22);
        System.out.println(numList);

        numList.selectionSort();
        System.out.println(numList);
    }
}
