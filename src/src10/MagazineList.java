package src10;

import src8.Sorting;
import java.util.ArrayList;
public class MagazineList
{
   private MagazineNode list;

   public MagazineList() { list = null; }
   //  Creates a new MagazineNode object and adds it to the end of the linked list.
   public void add(Magazine mag)
   {
      MagazineNode node = new MagazineNode(mag);
      MagazineNode current;

      if (list == null)
         list = node;
      else
      {
         current = list;
         while (current.next != null)
            current = current.next;
         current.next = node;
      }
   }
   public void insert(int index, Magazine newmagazine)
   {
        MagazineNode node = new MagazineNode(newmagazine);
        MagazineNode current = list;
      if(index==0)
      {
         list = node;
         list.next=current;
      }
      else
      {
         int x=0;
         while (x<index-1)
         {
            current = current.next;
            x++;
         }
         node.next = current.next;
         current.next = node;
      }
   }
   public void  delete(Magazine delNode)
   {
      MagazineNode current = list;
      if(current.getMagazine().equals(String.valueOf(delNode)))
      {
         list=current.next;
      }
      else {
         while (!((current.next.getMagazine()).equals(String.valueOf(delNode))))
            current = current.next;
         current.next = current.next.next;
      }}
   public void Sort()
   {
      int n =0;
      ArrayList<Comparable> magazines = new ArrayList();
      while (list!=null)
      {
         magazines.add(n, (Comparable) list.getMagazine());
         list = list.next;
         n++;
      }
      n = 0;
      Comparable[] list = new Comparable[magazines.size()];
      while (n<magazines.size())
      {
         list[n] = magazines.get(n);
         n++;
      }
      Sorting.selectionSort(list);
      for (Comparable com : list)
         System.out.println(com);
   }

   public String toString()
   {
      String result = "";

      MagazineNode current = list;

      while (current != null)
      {
         result += current.magazine + "\n";
         current = current.next;
      }
      return result;
   }

   private class MagazineNode
   {
      public Magazine magazine;
      public MagazineNode next;

      public MagazineNode(Magazine mag)
      {
         magazine = mag;
         next = null;
      }
      public String getMagazine()
      {
         return String.valueOf(magazine);
      }
   }
}