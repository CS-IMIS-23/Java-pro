package src10;
import java.text.NumberFormat;

public class DVD
{
    private String title,director;
    private int year;
    private  double cost;
    private boolean bluray;

    public DVD(String title, String director, int year, double cost, boolean bluray)
    {
        this.bluray = bluray;
        this.cost = cost;
        this.director=director;
        this.title=title;
        this.year=year;
    }
    public String toString()
    {
        NumberFormat fmt = NumberFormat.getCurrencyInstance();
        String description;

        description = fmt.format(cost)+"\t"+year+"\t";
        description += title+"\t"+director;
        if(bluray)
            description +="\t"+"Blu-ray";
        return description;
    }
}
